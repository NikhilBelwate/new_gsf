package com.quinnox.epod.constants;

import android.os.Environment;

import java.io.File;

/**
 * Created by Shabbir on 20-07-2015.
 */
public class EpodConstants {

    public static final String APP_DIRECTORY = "ePOD";

    public class JSONParams {
        public static final String DATA ="Data";
        public static final String NAME ="name";
        public static final String LATITUDE ="latitude";
        public static final String LONGITUDE ="longitude";
        public static final String ODOMETER ="odometer";
        public static final String REEFERFUELPERCERNT ="percent";
        public static final String DISTANCE ="distance";
        public static final String GEOFENCE ="geofence";
        public static final String INSIDE_NAMES ="inside.names";
        public static final String TYPE ="type";
        public static final String COMP ="comp";
        public static final String AMBIENTAIR ="aat";
        public static final String SUPPLYAIR ="sat";
        public static final String TEMPERATURE ="temp";
        public static final String SETPOINT  ="set";
    }

    public static final String PDFPATH = Environment.getExternalStorageDirectory().getAbsolutePath() +
            File.separator + EpodConstants.APP_DIRECTORY + File.separator + "/invoice.pdf";

    public static final String DELIVERYSLIPPDF = Environment.getExternalStorageDirectory().getAbsolutePath() +
            File.separator + EpodConstants.APP_DIRECTORY + File.separator + "/delivery_slip.pdf";

    public static final String CREDITMEMO = Environment.getExternalStorageDirectory().getAbsolutePath() +
            File.separator + EpodConstants.APP_DIRECTORY + File.separator + "/credit_memo.pdf";}
