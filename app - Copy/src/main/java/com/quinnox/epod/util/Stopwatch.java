package com.quinnox.epod.util;

import java.util.Timer;

/**
 * Created by NikhilB on 17-07-2015.
 */
public class Stopwatch {
    private long startTime = 0;
    private boolean running = false;
    private long currentTime = 0;

    private long startHour=0;
    private long startMin=0;
    private long startSec=0;
    public Stopwatch(String startTime)
    {
        String[] splitTime=startTime.split(":");
        if(splitTime.length>0)
        startHour=Long.parseLong(splitTime[0]);
        if(splitTime.length>1)
            startMin=Long.parseLong(splitTime[1]);
        if(splitTime.length>2)
            startSec=Long.parseLong(splitTime[2]);

    }
    public Stopwatch(){

    }
    public void start() {
        this.startTime = System.currentTimeMillis();
        this.running = true;
    }


    public void stop() {
        this.running = false;
    }

    public void pause() {
        this.running = false;
        currentTime = System.currentTimeMillis() - startTime;
    }
    public void resume() {
        this.running = true;
        this.startTime = System.currentTimeMillis() - currentTime;
    }


    //elaspsed time in milliseconds
    public long getElapsedTimeMili() {
        long elapsed = 0;
        if (running) {
            elapsed =((System.currentTimeMillis() - startTime)/100) % 1000 ;
        }
        return elapsed;
    }


    //elaspsed time in seconds
    public long getElapsedTimeSecs() {
        long elapsed = 0;
        if (running) {
            elapsed = ((System.currentTimeMillis() - startTime) / 1000) % 60;
        }
        return startSec+elapsed;
    }

    //elaspsed time in minutes
    public long getElapsedTimeMin() {
        long elapsed = 0;
        if (running) {
            elapsed = (((System.currentTimeMillis() - startTime) / 1000) / 60 ) % 60;
        }
        return startMin+elapsed;
    }

    //elaspsed time in hours
    public long getElapsedTimeHour() {
        long elapsed = 0;
        if (running) {
            elapsed = ((((System.currentTimeMillis() - startTime) / 1000) / 60 ) / 60);
        }
        return startHour+elapsed;
    }
}
