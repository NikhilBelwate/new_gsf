package com.quinnox.epod.network;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.security.KeyStore;

/**
 * Created by Shabbir on 20-07-2015.
 */
public class HttpUtils {

    public static HttpClient getNewHttpClient(boolean argBypassCertAuth) {
        return getNewHttpClient(argBypassCertAuth, new BasicHttpParams());
    }


    public static HttpClient getNewHttpClient(boolean argBypassCertAuth, HttpParams argParams) {

        if (!argBypassCertAuth) {
            return new DefaultHttpClient(argParams);
        }

        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new EasySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(
                    SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(argParams, registry);

            return new DefaultHttpClient(ccm, argParams);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
