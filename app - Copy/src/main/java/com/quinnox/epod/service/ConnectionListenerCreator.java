package com.quinnox.epod.service;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.quinnox.epod.beans.BaseGsonBean;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class ConnectionListenerCreator<T extends BaseGsonBean> {

    public Response.Listener createGetResponseListener(final int argRequestType, final IResponse argIResponse) {
        return new Response.Listener<T>() {
            @Override
            public void onResponse(T argResponseBean) {
                ResponseHandler<T> handler = new ResponseHandler<>(argRequestType, argIResponse);
                handler.sendResponse(argResponseBean);
            }
        };
    }

    public Response.Listener<String> createPostResponseListener(final int argRequestType, final IResponse argIResponse) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String argPostResponse) {
                ResponseHandler<T> handler = new ResponseHandler<T>(argRequestType, argIResponse);
                handler.sendResponse(argPostResponse);
            }
        };
    }

    public Response.ErrorListener createErrorListener(final int argRequestType, final IResponse argIResponse) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError argError) {
                ResponseHandler<T> handler = new ResponseHandler<T>(argRequestType, argIResponse);
                handler.reportError(argError);
            }
        };
    }
}
