package com.quinnox.epod.service;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyHelper {

    private static final int DEFAULT_DISK_USAGE_BYTES = 25 * 1024 * 1024;

    private static RequestQueue mRequestQueue;

    private VolleyHelper() {
        // no instances
    }

    public static void init(Context argcontext) {
        mRequestQueue =  Volley.newRequestQueue(argcontext);
    }

    public static RequestQueue getRequestQueue(){ return mRequestQueue; }
}
