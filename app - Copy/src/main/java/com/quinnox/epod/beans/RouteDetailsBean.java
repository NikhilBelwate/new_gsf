package com.quinnox.epod.beans;

import java.util.ArrayList;

/**
 * Created by NikhilB on 13-07-2015.
 */
public class RouteDetailsBean extends BaseGsonBean {


    public ArrayList<RouteBean> getData() {
        return data;
    }

    public void setData(ArrayList<RouteBean> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RouteDetailsBean{" +
                "data=" + data +
                '}';
    }

    private ArrayList<RouteBean> data;
}
