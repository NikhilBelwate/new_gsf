package com.quinnox.epod.beans;

/**
 * Created by NikhilB on 13-07-2015.
 */
public class DeliveryBean extends BaseGsonBean {
    private String desc;

    private String price;

    private String productid;

    private String qty;

    public String getDesc ()
    {
        return desc;
    }

    public void setDesc (String desc)
    {
        this.desc = desc;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getProductid ()
    {
        return productid;
    }

    public void setProductid (String productid)
    {
        this.productid = productid;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    @Override
    public String toString()
    {
        return "ClassPojo<DeliveryBean> [desc = "+desc+", price = "+price+", productid = "+productid+", qty = "+qty+"]";
    }
}
