package com.quinnox.epod.beans;

/**
 * Created by NikhilB on 13-07-2015.
 */
public class RouteBean extends BaseGsonBean {
    private String stop;

    private String starttime;

    private String address;

    private String clientid;

    private String telno;

    private DeliveryBean[] delivery;

    private String arrivaltime;

    public String getStop ()
    {
        return stop;
    }

    public void setStop (String stop)
    {
        this.stop = stop;
    }

    public String getStarttime ()
    {
        return starttime;
    }

    public void setStarttime (String starttime)
    {
        this.starttime = starttime;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getClientid ()
    {
        return clientid;
    }

    public void setClientid (String clientid)
    {
        this.clientid = clientid;
    }

    public String getTelno ()
    {
        return telno;
    }

    public void setTelno (String telno)
    {
        this.telno = telno;
    }

    public DeliveryBean[] getDeliveryBean ()
    {
        return delivery;
    }

    public void setDeliveryBean (DeliveryBean[] delivery)
    {
        this.delivery = delivery;
    }

    public String getArrivaltime ()
    {
        return arrivaltime;
    }

    public void setArrivaltime (String arrivaltime)
    {
        this.arrivaltime = arrivaltime;
    }

    @Override
    public String toString()
    {
        return "ClassPojo<RouteBean> [stop = "+stop+", starttime = "+starttime+", address = "+address+", telno = "+telno+", DeliveryBean = "+delivery+", arrivaltime = "+arrivaltime+"]";
    }
}
