package com.quinnox.epod.db;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class DBHelperUser {

    public static final String	TABLE_USER		= "user";

    public static final String	COL_USERNAME	= "username";

    public static final String createTable() {
        StringBuffer _sBuff = new StringBuffer();
        _sBuff.append("create table ");
        _sBuff.append(TABLE_USER);
        _sBuff.append(" (");
        _sBuff.append(" text, ");
        _sBuff.append(COL_USERNAME);
        _sBuff.append(" text);");
        return _sBuff.toString();
    }


}
