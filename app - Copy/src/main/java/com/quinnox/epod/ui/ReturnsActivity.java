package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.zxing.client.android.Intents;
import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.util.TempDeliveryUtility;

/**
 * Created by AradhanaV on 7/24/2015.
 */
public class ReturnsActivity extends EpodBaseActivity {

    private Button btnSignOff, btnScan;

    private EditText edit_qty1_ret, edit_qty1_prod_desc, edit_qty2_ret, edit_qty2_prod_desc, edit_qty3_ret, edit_qty3_prod_desc,edit_qty1_prod_barcode,edit_qty2_prod_barcode,edit_qty3_prod_barcode;
    private String qty1_ret, qty2_ret, qty3_ret, qty1_detail, qty2_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_returns);
        EpodApp.setActionBar(this);

        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();
    }

    private void _init() {
        btnSignOff = (Button) findViewById(R.id.btn_sign_off);
        btnScan = (Button) findViewById(R.id.btn_scan);

        edit_qty1_ret = (EditText) findViewById(R.id.edit_qty1_ret);
        edit_qty1_prod_barcode=(EditText)findViewById(R.id.edit_qty1_prod_barcode);
        edit_qty1_prod_desc = (EditText) findViewById(R.id.edit_qty1_prod_desc);

        edit_qty2_ret = (EditText) findViewById(R.id.edit_qty2_ret);
        edit_qty2_prod_barcode=(EditText)findViewById(R.id.edit_qty2_prod_barcode);
        edit_qty2_prod_desc = (EditText) findViewById(R.id.edit_qty2_prod_desc);

        edit_qty3_ret = (EditText) findViewById(R.id.edit_qty3_ret);
        edit_qty3_prod_barcode=(EditText)findViewById(R.id.edit_qty3_prod_barcode);
        edit_qty3_prod_desc = (EditText) findViewById(R.id.edit_qty3_prod_desc);
    }

    private void _wireEvents() {

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", Intents.Scan.MODE);
                    //     intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 0);

                } catch (Exception e) {
                    //          Log.v("ReturnsActivity", "scan e::" + e);
                }

            }
        });

        btnSignOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ReturnsActivity.this, SignatureCaptureActivity.class));
              //  finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                qty1_ret = edit_qty1_ret.getText().toString();
                qty2_ret = edit_qty2_ret.getText().toString();
                qty3_ret = edit_qty3_ret.getText().toString();


                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.v("ReturnsActivity", "Scanned contents is::" + contents + " format: " + format);

                if (edit_qty1_prod_barcode.getText().toString().equals("")) {
                    edit_qty1_prod_barcode.setText(contents);
                    edit_qty1_prod_desc.setText("Milk container");
                    edit_qty1_ret.setText(String.valueOf(1));
                    qty1_ret = "";

                }
                if (edit_qty2_prod_barcode.getText().toString().equals("") && !contents.equals(edit_qty1_prod_barcode.getText().toString())) {
                    edit_qty2_prod_barcode.setText(contents);
                    edit_qty2_prod_desc.setText("Carboard boxes");
                    edit_qty2_ret.setText(String.valueOf(1));
                    qty2_ret = "";

                }
                if (edit_qty3_prod_barcode.getText().toString().equals("") && !contents.equals(edit_qty1_prod_barcode.getText().toString()) && !contents.equals(edit_qty2_prod_barcode.getText().toString())) {
                    edit_qty3_prod_barcode.setText(contents);
                    edit_qty3_prod_desc.setText("Cold drink cans");
                    edit_qty3_ret.setText(String.valueOf(1));
                    qty3_ret = "";

                }

                //to match the contents of barcode and increment the count of respective product
                if (!edit_qty1_prod_barcode.getText().toString().equals("") || !edit_qty2_prod_barcode.getText().toString().equals("") || !edit_qty3_prod_barcode.getText().toString().equals("")) {

                    if (contents.equals(edit_qty1_prod_barcode.getText().toString()) && !qty1_ret.equals("")) {
                        qty1_ret = edit_qty1_ret.getText().toString();
                        qty1_ret = String.valueOf(Integer.parseInt(qty1_ret) + 1);
                        edit_qty1_ret.setText(qty1_ret);

                    } else if (contents.equals(edit_qty2_prod_barcode.getText().toString()) && !qty2_ret.equals("")){
                        qty2_ret = edit_qty2_ret.getText().toString();
                        qty2_ret = String.valueOf(Integer.parseInt(qty2_ret) + 1);
                        edit_qty2_ret.setText(qty2_ret);

                    } else if (contents.equals(edit_qty3_prod_barcode.getText().toString()) && !qty3_ret.equals("")) {
                        qty3_ret = edit_qty3_ret.getText().toString();
                        qty3_ret = String.valueOf(Integer.parseInt(qty3_ret) + 1);
                        edit_qty3_ret.setText(qty3_ret);

                    }
                }

            }
        }
    }
}

