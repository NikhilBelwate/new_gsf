package com.quinnox.epod.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quinnox.epod.R;

public class LoginActivity extends Activity{

    private EditText edt_txt_driverId;
    private Button btn_login;
    private String driverId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //to initialize all components
        _init();
        // wire all events
        _wireEvents();



    }
    //method to initialize all components
    private void _init() {
        edt_txt_driverId=(EditText)findViewById(R.id.edt_text_driver_id);
        btn_login=(Button)findViewById(R.id.btn_login);
        driverId= edt_txt_driverId.getText().toString().trim();

    }
    //method to wire all events
    public void _wireEvents() {

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driverId= edt_txt_driverId.getText().toString().trim();

                if (driverId.length() > 0) {
                    Log.v("LoginActivity", "login successful ");
                    Intent intent=new Intent(LoginActivity.this,PreCheckListActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    edt_txt_driverId.setHint(getString(R.string.driver_id_required));
                    edt_txt_driverId.setHintTextColor(getResources().getColor(R.color.error_red));

                }

            }
        });
    }

}
