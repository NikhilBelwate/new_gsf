package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteBean;
import com.quinnox.epod.util.TempDeliveryUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimerActivity extends EpodBaseActivity {

    private TextView startTime;
    private TextView endTime;
    private TextView clientId;
    private TextView stopAddress;
    private TextView message;
    private Button arrivalBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        _init();
        _wireEvents();
        EpodApp.setActionBar(this);
    }

    private void _init() {

        startTime = (TextView) findViewById(R.id.txt_start_time);
        endTime = (TextView) findViewById(R.id.txt_arrival_time);
        clientId = (TextView) findViewById(R.id.txt_client_id);
        stopAddress = (TextView) findViewById(R.id.txt_client_address);
        message = (TextView) findViewById(R.id.txt_Message);

        arrivalBtn = (Button) findViewById(R.id.btn_arrival);


        RouteBean routeBean = (RouteBean) getIntent().getSerializableExtra("route");
        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();
        Log.v("TimerActivity","duration:"+ getIntent().getStringExtra("duration"));
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Calendar c = new GregorianCalendar();

        Log.v("TimerActivity","mins from intent: "+(getIntent().getStringExtra("duration")).substring(3,5));

        Log.v("TimerActivity","routeBean mins "+(routeBean.getStarttime()));
      //  int hrs= Integer.parseInt(getIntent().getStringExtra("duration").substring(0,2))+ Integer.parseInt(sdf.format(routeBean.getStarttime()).substring(0,2));
        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(getIntent().getStringExtra("duration").substring(0,2))+ Integer.parseInt((routeBean.getStarttime()).substring(0,2)));
        c.set(Calendar.MINUTE,Integer.parseInt(getIntent().getStringExtra("duration").substring(3,5))+Integer.parseInt((routeBean.getStarttime()).substring(3,5)));
        c.set(Calendar.SECOND, Integer.parseInt(getIntent().getStringExtra("duration").substring(6,8)));
        Date d = c.getTime();



        Log.v("TimerActivity","Start point: " +sdf.format(d));

        Log.v("TimerActivity","Nearest whole minute: " +sdf.format(toNearestWholeMinute(d)));
        Log.v("TimerActivity","Nearest whole hour: " + sdf.format(d));

        message.setText(getIntent().getStringExtra("duration"));

        stopAddress.setText(routeBean.getAddress() + "\n" + routeBean.getTelno());
        startTime.setText(routeBean.getStarttime());
      //  routeBean.setArrivaltime(sdf.format(toNearestWholeMinute(d)).substring(0,5));


        tempDeliveryUtility.setActualArrivalTime(sdf.format(toNearestWholeMinute(d)).substring(0,5));
        TempDeliveryUtility.getInstance().setActualArrivalTime(sdf.format(toNearestWholeMinute(d)).substring(0,5));
        endTime.setText(sdf.format(toNearestWholeMinute(d)).substring(0,5));
        clientId.setText(routeBean.getClientid());
    }
//For stop watch


    //method to wire all events
    public void _wireEvents() {
        arrivalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TimerActivity.this, CountAndScanModeActivity.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                startActivity(intent);

            }
        });
    }


    private static Date toNearestWholeMinute(Date d) {
        Calendar c = new GregorianCalendar();
        c.setTime(d);

        if (c.get(Calendar.SECOND) > 0)
            c.add(Calendar.MINUTE, 1);

        c.set(Calendar.SECOND, 0);

        return c.getTime();
    }

    static Date toNearestWholeHour(Date d) {
        Calendar c = new GregorianCalendar();
        c.setTime(d);

        if (c.get(Calendar.MINUTE) >=30)
            c.add(Calendar.HOUR, 1);

        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        return c.getTime();
    }

}
