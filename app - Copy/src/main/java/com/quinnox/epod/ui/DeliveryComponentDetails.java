package com.quinnox.epod.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.artifex.mupdflib.MuPDFActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.quinnox.epod.R;
import com.quinnox.epod.constants.EpodConstants;

import java.io.File;

/**
 * Created by AradhanaV on 9/10/2015.
 */
public class DeliveryComponentDetails extends FragmentActivity {
    Context context;
    public static final String TYPE = "type";
    String componentType;
    Button btnBack;
    LinearLayout delivery_slip, credit_memo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        context=getApplicationContext();
        super.onCreate(savedInstanceState);
        //getActionBar().hide();

        componentType = getIntent().getStringExtra(TYPE);
        Log.v("DeliveryComDetails", "componentType:" + componentType);
        //to initialize all components
        _init();
        // wire all events
        _wireEvents();

    }

    private void _init() {
        if (componentType.equals("Delivery Content")) {
            setContentView(R.layout.activity_delivery_content);
            btnBack = (Button) findViewById(R.id.btn_back);
        } else if (componentType.equals("Delivery Document")) {
            setContentView(R.layout.activity_delivery_doc);

            delivery_slip = (LinearLayout)findViewById(R.id.ll_delivery_slip);
            delivery_slip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MuPDFActivity.class);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(EpodConstants.DELIVERYSLIPPDF)), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);

                }
            });

            credit_memo = (LinearLayout) findViewById(R.id.ll_credit_memo);
            credit_memo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MuPDFActivity.class);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(EpodConstants.CREDITMEMO)), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);

                }
            });
            btnBack = (Button) findViewById(R.id.btn_back);
        } else if (componentType.equals("Restaurant Details")) {
            setContentView(R.layout.activity_restaurant_details);
            btnBack = (Button) findViewById(R.id.btn_back);
        } else {
            setContentView(R.layout.activity_specific_info);

            LatLng QCS = new LatLng(37.333497, -121.894798);
            GoogleMap googleMap;
            try {


                googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                        .getMap();

                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                Marker TP = googleMap.addMarker(new MarkerOptions().position(QCS).title("You are here!"));
               TP.setVisible(true);
                //Move the camera to the user's location and zoom in!
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(QCS, 18.0f));

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("map error",e.toString());
            }
            btnBack = (Button) findViewById(R.id.btn_back);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        int windowWidth = size.x;
        int windowHeight = size.y;

        double widthconst = 0.90;
        double hightconst = 0.80;
        widthconst = widthconst * windowWidth;
        hightconst = hightconst * windowHeight;
        windowHeight = (int) (hightconst);
        windowWidth = (int) (widthconst);
        getWindow().setLayout(windowWidth, windowHeight);
    }

    private void _wireEvents() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Intent intent = new Intent(DeliveryComponentDetails.this, StartJourneyActivity.class);
              //  intent.putExtra("route", getIntent().getSerializableExtra("route"));
                //startActivity(intent);
                finish();
            }
        });

    }
}
