package com.quinnox.epod.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.constants.EpodConstants;
import com.quinnox.epod.ui.view.SignatureView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Shabbir on 16-07-2015.
 */
public class SignatureCaptureActivity  extends EpodBaseActivity implements View.OnClickListener
{
    Button btnCancel;
    Button btnSubmit;
    SignatureView truckDriverSignatureView;
    SignatureView storeManagerSignatureView;

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.signature_capture);
        truckDriverSignatureView = (SignatureView)findViewById( R.id.truckDriverSignature );
        storeManagerSignatureView = (SignatureView)findViewById( R.id.storeManagerSignature );
        btnCancel = (Button)findViewById( R.id.btnCancel );
        btnSubmit = (Button)findViewById( R.id.btnSubmit );

        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        EpodApp.setActionBar(this);
    }

    @Override
    public void onClick( View argView )
    {
        if(argView.getId() == R.id.btnSubmit)
        {
            saveSignature(truckDriverSignatureView, "truckDriver");
            saveSignature(storeManagerSignatureView, "storeManager");
            startActivity(new Intent(SignatureCaptureActivity.this, DeliverySummaryActivity.class));
            finish();
        }
        else if(argView.getId() == R.id.btnCancel)
        {
            truckDriverSignatureView.clear();
            storeManagerSignatureView.clear();
        }
    }

    private void saveSignature(SignatureView argSignatureView, String argFileName) {
        Bitmap bitmap = Bitmap.createBitmap( argSignatureView.getWidth(), argSignatureView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        argSignatureView.draw(canvas);
        FileOutputStream fos;
        try
        {
            File file = new File(Environment.getExternalStorageDirectory(), EpodConstants.APP_DIRECTORY
                    + File.separator + argFileName + ".png");
            fos = new FileOutputStream( file );
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        }
        catch( FileNotFoundException e )
        {
            Log.e("SignatureCaptActivity", "onClick()-Error creating Signature file");
            e.printStackTrace();
        }
        catch( IOException e )
        {
            Log.e( "SignatureCaptActivity", "onClick()-Error flushing/closing Signature file" );
            e.printStackTrace();
        }
    }
}