package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;

/**
 * Created by AradhanaV on 7/15/2015.
 */
public class PreCheckListActivity extends EpodBaseActivity {

    private ImageView mImg_cross1, mImg_check1;
    private ImageView mImg_cross2, mImg_check2;
    private ImageView mImg_cross3, mImg_check3;
    private LinearLayout ll_option1_no, ll_option2_no, ll_option3_no, ll_option1_yes, ll_option2_yes, ll_option3_yes;
    private Button mBtnRouteList;
    Button preCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_check_list);

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();
        EpodApp.setActionBar(this);
//        Log.i("selected route data", getIntent().getSerializableExtra("route").toString());

        // temporary link to go to start journey activity
        // gotoStartJourneyActivity();

    }

    //method to initialize all components
    private void _init() {

        //to initialize 1st questions elements
        mImg_cross1 = (ImageView) findViewById(R.id.img1_cross);
        mImg_check1 = (ImageView) findViewById(R.id.img1_check);
        ll_option1_no = (LinearLayout) findViewById(R.id.ll_option1_no);
        ll_option1_yes = (LinearLayout) findViewById(R.id.ll_option1_yes);

        //to initialize 2nd questions elements
        mImg_cross2 = (ImageView) findViewById(R.id.img2_cross);
        mImg_check2 = (ImageView) findViewById(R.id.img2_check);
        ll_option2_no = (LinearLayout) findViewById(R.id.ll_option2_no);
        ll_option2_yes = (LinearLayout) findViewById(R.id.ll_option2_yes);


        //to initialize 3rd questions elements
        mImg_cross3 = (ImageView) findViewById(R.id.img3_cross);
        mImg_check3 = (ImageView) findViewById(R.id.img3_check);
        ll_option3_no=(LinearLayout)findViewById(R.id.ll_option3_no);
        ll_option3_yes=(LinearLayout)findViewById(R.id.ll_option3_yes);

        preCheck = (Button) findViewById(R.id.btn_route_list);

        mImg_cross1.setTag("gray_cross");
        mImg_check1.setTag("gray_check");
    }

    //method to wire all events
    public void _wireEvents() {


        //to select 1st questions option--

        ll_option1_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });

        ll_option1_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_cross1.getTag() != null && mImg_cross1.getTag().toString().equals("gray_cross")) {
                    mImg_cross1.setImageResource(R.drawable.red_cross_icon);
                    mImg_cross1.setTag("red_cross");

                    if (mImg_check1.getTag() != null && mImg_check1.getTag().toString().equals("green_check")) {
                        mImg_check1.setImageResource(R.drawable.gray_checkmark);
                        mImg_check1.setTag("gray_check");

                    }
                } else {
                    mImg_cross1.setImageResource(R.drawable.gray_cross_icon);
                    mImg_cross1.setTag("gray_cross");
                }


            }
        });


        ll_option1_yes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });
        ll_option1_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_check1.getTag() != null && mImg_check1.getTag().toString().equals("gray_check")) {
                    mImg_check1.setImageResource(R.drawable.green_checkmark);
                    mImg_check1.setTag("green_check");
                    if (mImg_cross1.getTag() != null && mImg_cross1.getTag().toString().equals("red_cross")) {
                        mImg_cross1.setImageResource(R.drawable.gray_cross_icon);
                        mImg_cross1.setTag("gray_cross");
                    }

                } else {
                    mImg_check1.setImageResource(R.drawable.gray_checkmark);
                    mImg_check1.setTag("gray_check");
                }

            }
        });


        //to select 2nd questions option--
        ll_option2_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });
        ll_option2_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_cross2.getTag() != null && mImg_cross2.getTag().toString().equals("gray_cross")) {
                    mImg_cross2.setImageResource(R.drawable.red_cross_icon);
                    mImg_cross2.setTag("red_cross");

                    if (mImg_check2.getTag() != null && mImg_check2.getTag().toString().equals("green_check")) {
                        mImg_check2.setImageResource(R.drawable.gray_checkmark);
                        mImg_check2.setTag("gray_check");

                    }
                } else {
                    mImg_cross2.setImageResource(R.drawable.gray_cross_icon);
                    mImg_cross2.setTag("gray_cross");
                }


            }
        });
        ll_option2_yes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });
        ll_option2_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_check2.getTag() != null && mImg_check2.getTag().toString().equals("gray_check")) {
                    mImg_check2.setImageResource(R.drawable.green_checkmark);
                    mImg_check2.setTag("green_check");
                    if (mImg_cross2.getTag() != null && mImg_cross2.getTag().toString().equals("red_cross")) {
                        mImg_cross2.setImageResource(R.drawable.gray_cross_icon);
                        mImg_cross2.setTag("gray_cross");
                    }

                } else {
                    mImg_check2.setImageResource(R.drawable.gray_checkmark);
                    mImg_check2.setTag("gray_check");
                }

            }
        });


        //to select 3rd questions option--
        ll_option3_yes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });
        ll_option3_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_cross3.getTag() != null && mImg_cross3.getTag().toString().equals("gray_cross")) {
                    mImg_cross3.setImageResource(R.drawable.red_cross_icon);
                    mImg_cross3.setTag("red_cross");

                    if (mImg_check3.getTag() != null && mImg_check3.getTag().toString().equals("green_check")) {
                        mImg_check3.setImageResource(R.drawable.gray_checkmark);
                        mImg_check3.setTag("gray_check");

                    }
                } else {
                    mImg_cross3.setImageResource(R.drawable.gray_cross_icon);
                    mImg_cross3.setTag("gray_cross");
                }


            }
        });
        ll_option3_yes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    v.performClick();
                }
            }
        });
        ll_option3_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mImg_check3.getTag() != null && mImg_check3.getTag().toString().equals("gray_check")) {
                    mImg_check3.setImageResource(R.drawable.green_checkmark);
                    mImg_check3.setTag("green_check");
                    if (mImg_cross3.getTag() != null && mImg_cross3.getTag().toString().equals("red_cross")) {
                        mImg_cross3.setImageResource(R.drawable.gray_cross_icon);
                        mImg_cross3.setTag("gray_cross");
                    }

                } else {
                    mImg_check3.setImageResource(R.drawable.gray_checkmark);
                    mImg_check3.setTag("gray_check");
                }

            }
        });

        preCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreCheckListActivity.this, RouteSummaryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void gotoStartJourneyActivity() {
        mBtnRouteList = (Button) findViewById(R.id.btn_route_list);
        mBtnRouteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PreCheckListActivity.this, StartJourneyActivity.class));
            }
        });


    }
}
