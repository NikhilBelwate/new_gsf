package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.util.TempDeliveryUtility;

/**
 * Created by AradhanaV on 7/20/2015.
 */
public class DeliveryDetailsActivity extends EpodBaseActivity {

    private Button btnCollectReturns;
    private TextView qty1_delivered, qty2_delivered, qty3_delivered;
    private TextView qty1Desc, qty1Barcode, qty2Desc, qty2Barcode, qty3Desc, qty3Barcode;
    private TextView qty1Price, qty2Price, qty3Price;
    private TextView qty1Total, qty2Total, qty3Total;
    private String mtxtBarcode1, mtxtBarcode2, mtxtBarcode3;
    private TextView txt_total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_details);
        EpodApp.setActionBar(this);

        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();


    }

    private void _init() {

        btnCollectReturns = (Button) findViewById(R.id.btn_collect_returns);
        qty1_delivered = (TextView) findViewById(R.id.txt_qty1_delivered);
        qty2_delivered = (TextView) findViewById(R.id.txt_qty2_delivered);
        qty3_delivered = (TextView) findViewById(R.id.txt_qty3_delivered);

        qty1Desc = (TextView) findViewById(R.id.txt_qty1_desc);
        qty2Desc = (TextView) findViewById(R.id.txt_qty2_desc);
        qty3Desc = (TextView) findViewById(R.id.txt_qty3_desc);

        qty1Barcode = (TextView) findViewById(R.id.txt_barcode1);
        qty2Barcode = (TextView) findViewById(R.id.txt_barcode2);
        qty3Barcode = (TextView) findViewById(R.id.txt_barcode3);

        qty1Price = (TextView) findViewById(R.id.txt_qty1_unit_price);
        qty2Price = (TextView) findViewById(R.id.txt_qty2_unit_price);
        qty3Price = (TextView) findViewById(R.id.txt_qty3_unit_price);

        qty1Total = (TextView) findViewById(R.id.txt_qty1_total);
        qty2Total = (TextView) findViewById(R.id.txt_qty2_total);
        qty3Total = (TextView) findViewById(R.id.txt_qty3_total);

        txt_total=(TextView)findViewById(R.id.txt_cal_total);

        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        mtxtBarcode1 = qty1Barcode.getText().toString();
        mtxtBarcode2 = qty2Barcode.getText().toString();
        mtxtBarcode3 = qty3Barcode.getText().toString();

        qty1Desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode1));
        qty2Desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode2));
        qty3Desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode3));

        qty1_delivered.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode1)));
        qty2_delivered.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode2)));
        qty3_delivered.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode3)));

        qty1Price.setText(String.valueOf(tempDeliveryUtility.getPrice(mtxtBarcode1)));
        qty2Price.setText(String.valueOf(tempDeliveryUtility.getPrice(mtxtBarcode2)));
        qty3Price.setText(String.valueOf(tempDeliveryUtility.getPrice(mtxtBarcode3)));

        qty1Total.setText(String.valueOf(tempDeliveryUtility.getTotalPrice(mtxtBarcode1)));
        qty2Total.setText(String.valueOf(tempDeliveryUtility.getTotalPrice(mtxtBarcode2)));
        qty3Total.setText(String.valueOf(tempDeliveryUtility.getTotalPrice(mtxtBarcode3)));
        txt_total.setText("$"+(tempDeliveryUtility.getInvoiceTotal()));
        Log.v("DeliveryDetailsActivity", "total::" + tempDeliveryUtility.getInvoiceTotal());


    }

    private void _wireEvents() {

        btnCollectReturns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DeliveryDetailsActivity.this, ReturnsActivity.class));
              //  finish();
            }
        });

    }

}
