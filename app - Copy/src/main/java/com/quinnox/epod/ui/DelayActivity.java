package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;

/**
 * Created by Aradhanav on 9/11/2015.
 */
public class DelayActivity extends EpodBaseActivity implements AdapterView.OnItemSelectedListener {

    Button btnSubmit, btnCancel;
    Spinner spinCauseOfDelay, spinDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delay);
        EpodApp.setActionBar(this);

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();


    }


    private void _init() {
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnCancel = (Button) findViewById(R.id.btnCancel);


        spinCauseOfDelay = (Spinner) findViewById(R.id.spinner_causes);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> causesAdapter = ArrayAdapter.createFromResource(this,
                R.array.delay_causes, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        causesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinCauseOfDelay.setAdapter(causesAdapter);
        spinCauseOfDelay.setOnItemSelectedListener(this);


        spinDetails = (Spinner) findViewById(R.id.spinner_details);
        spinDetails.setEnabled(false);

        //spinDetails.setOnItemSelectedListener(this);
    }

    private void _wireEvents() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason="Cause of delay - "+spinCauseOfDelay.getSelectedItem();
                reason=reason+"\n Details - "+spinDetails.getSelectedItem();
                //Toast.makeText(getApplicationContext(),reason,Toast.LENGTH_LONG).show();
                setReason(reason);
                Intent intent = new Intent(DelayActivity.this, TimerActivity.class);
                intent.putExtra("route", DelayActivity.this.getIntent().getSerializableExtra("route"));
                intent.putExtra("duration", getIntent().getStringExtra("duration"));
                startActivity(intent);

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.v("DelayActivity", "Selected item: " + parent.getItemAtPosition(position));
        switch (position) {
            case 0:
                spinDetails.setEnabled(false);
                spinDetails.setVisibility(View.INVISIBLE);
                break;
            case 1:
                spinDetails.setEnabled(true);
                spinDetails.setVisibility(View.VISIBLE);
                ArrayAdapter<CharSequence> detailsAdapter = ArrayAdapter.createFromResource(this,
                        R.array.delay_details_planification, R.layout.spinner_item);
                detailsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinDetails.setAdapter(detailsAdapter);
                break;
            case 2:
                spinDetails.setEnabled(true);
                spinDetails.setVisibility(View.VISIBLE);
                ArrayAdapter<CharSequence> detailsAdapter1 = ArrayAdapter.createFromResource(this,
                        R.array.delay_details_puncture, R.layout.spinner_item);
                detailsAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinDetails.setAdapter(detailsAdapter1);
                break;

            case 3:
                spinDetails.setEnabled(true);
                spinDetails.setVisibility(View.VISIBLE);
                ArrayAdapter<CharSequence> detailsAdapter2 = ArrayAdapter.createFromResource(this,
                        R.array.delay_unloading, R.layout.spinner_item);
                detailsAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinDetails.setAdapter(detailsAdapter2);
                break;

            case 4:
                spinDetails.setEnabled(true);
                spinDetails.setVisibility(View.VISIBLE);
                ArrayAdapter<CharSequence> detailsAdapter3 = ArrayAdapter.createFromResource(this,
                        R.array.delay_traffic, R.layout.spinner_item);
                detailsAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinDetails.setAdapter(detailsAdapter3);
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
