package com.quinnox.epod.service;

import com.android.volley.VolleyError;
import com.quinnox.epod.beans.BaseGsonBean;

/**
 * Created by Shabbir on 09-07-2015.
 */
public class ResponseEntity {

    public static final int REQUEST_TEMPERATURE 		    = 1001;
    public static final int REQUEST_LOGIN 				    = 1002;
    public static final int REQUEST_DELIVERIES_FOR_TODAY	= 1003;

    private int mRequestType = 0;
    private String mPostResponse = null;
    private BaseGsonBean mGsonBean = null;
    private VolleyError mVolleyError = null;

    public ResponseEntity(int argRequestType, String argResponse) {
        mRequestType = argRequestType;
        mPostResponse = argResponse;
    }

    public ResponseEntity(int argRequestType, BaseGsonBean<?> argBaseBean) {
        mRequestType = argRequestType;
        mGsonBean = argBaseBean;
    }

    public ResponseEntity(int argRequestType, VolleyError argVolleyError) {
        mRequestType = argRequestType;
        mVolleyError = argVolleyError;
    }

    public int getRequestType() {
        return mRequestType;
    }

    public void setRequestType(int argRequestType) {
        mRequestType = argRequestType;
    }

    public String getPostResponse() {
        return mPostResponse;
    }

    public void setPostResponse(String argPostResponse) {
        mPostResponse = argPostResponse;
    }

    public BaseGsonBean getGsonBean(){
        return mGsonBean;
    }

    public void setBaseBean(BaseGsonBean argGsonBean){
        mGsonBean = argGsonBean;
    }
}
