package com.quinnox.epod.service;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
/**
 * Created by Shabbir on 09-07-2015.
 */
public class GsonRequest<T> extends Request<T> {
    private final Gson mGson;
    private final Class<T> mClass;
    private final Listener<T> mListener;

    public GsonRequest(String argUrl, Class<T> argClass, Listener<T> argSuccessListener, ErrorListener argErrorListener) {
        super(Method.GET, argUrl, argErrorListener);
        mClass = argClass;
        mListener = argSuccessListener;
        mGson = new Gson();
    }

    public GsonRequest(String argUrl, Listener<T> argSuccessListener, ErrorListener argErrorListener) {
        super(Method.GET, argUrl, argErrorListener);
        mListener = argSuccessListener;
        mClass = null;
        mGson = null;
    }

    public GsonRequest(int argMethod, String argUrl, Class<T> argClass, Listener<T> argSuccessListener, ErrorListener argErrorListener,
            Gson argGson) {
        super(argMethod, argUrl, argErrorListener);
        mClass = argClass;
        mListener = argSuccessListener;
        mGson = argGson;
    }

    @Override
    protected void deliverResponse(T argResponse) {
        mListener.onResponse(argResponse);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse argResponse) {
        try {
            String json = new String(argResponse.data, HttpHeaderParser.parseCharset(argResponse.headers));
            return Response.success(mGson.fromJson(json, mClass), getCacheEntry());
        } catch (Exception e) {
            System.out.println("ERROR (parseNetworkResponse) : " + e);
            return Response.error(new ParseError(e));
        }
    }
}