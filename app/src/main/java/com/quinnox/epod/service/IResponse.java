package com.quinnox.epod.service;

/**
 * Created by Shabbir on 09-07-2015.
 */
public interface IResponse {

    public void onSuccessResponse(ResponseEntity argResponseEntity);

    public void onErrorResponse(ResponseEntity argResponseEntity);
}
