package com.quinnox.epod.service;
/**
 * Created by Shabbir on 09-07-2015.
 */
public class URLFactory {

    // To get the weather details from open weather api
    public static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?units=imperial&appid=2c3d4fea6f38780642005aa8c93159d2&";

    // To get details of delivers to be done today
    public static final String DELIVERIES_TODAY_URL = "http://www.mocky.io/v2/55af294fbf6e05fc2266fb32";

    // To get details from iBright
    public static final String IBRIGHT_URL = "https://api.ibright.info/v2/rawdata/latest?AssetIDFilter=16277&orderby=WhenOccurred&LogNameFilter=position%2Ctemperature2%2Ctemperature1%2CreeferFuelLevel%2CpositionTimer%2CmotionStart%2CmotionStop&apikey=df4d2438-9d90-4b03-b025-98412018e3ad";


}
