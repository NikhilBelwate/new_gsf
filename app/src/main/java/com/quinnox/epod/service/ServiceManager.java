package com.quinnox.epod.service;

import com.android.volley.RequestQueue;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteDetailsBean;
import com.quinnox.epod.beans.TemperatureBean;
import com.quinnox.epod.network.EpodHttpClient;
import com.quinnox.epod.util.Util;

import java.io.InputStream;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class ServiceManager {

    private static ServiceManager mServiceManager;
    private static RequestQueue mRequestQueue = null;

    private ServiceManager(){
        mRequestQueue = VolleyHelper.getRequestQueue();
    }

    public static ServiceManager getInstance(){
        if(null == mServiceManager){
            mServiceManager = new ServiceManager();
        }
        return mServiceManager;
    }

    public void getTemperature(String argLocation, IResponse argIResponse){
        ConnectionListenerCreator connectionListenerCreator = new ConnectionListenerCreator();
        GsonRequest gsonRequest = new GsonRequest( URLFactory.WEATHER_API_URL+argLocation, TemperatureBean.class,
                connectionListenerCreator.createGetResponseListener(ResponseEntity.REQUEST_TEMPERATURE,argIResponse),
                connectionListenerCreator.createErrorListener(ResponseEntity.REQUEST_TEMPERATURE, argIResponse));
        mRequestQueue.add(gsonRequest);
    }
    public void getRoutes(IResponse argIResponse){
        ConnectionListenerCreator connectionListenerCreator = new ConnectionListenerCreator();
        GsonRequest gsonRequest = new GsonRequest( URLFactory.DELIVERIES_TODAY_URL, RouteDetailsBean.class,
                connectionListenerCreator.createGetResponseListener(ResponseEntity.REQUEST_DELIVERIES_FOR_TODAY,argIResponse),
                connectionListenerCreator.createErrorListener(ResponseEntity.REQUEST_DELIVERIES_FOR_TODAY, argIResponse));
        mRequestQueue.add(gsonRequest);
    }

    public String getTruckTemperature() {
        String response = "";
        EpodHttpClient httpClient = new EpodHttpClient(EpodApp.getContext());
        try {
            InputStream inputStream = httpClient.doGet(URLFactory.IBRIGHT_URL);
            response = Util.convertInputStreamToString(inputStream);
//            Log.d("SHABS", "iBright : "+response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
