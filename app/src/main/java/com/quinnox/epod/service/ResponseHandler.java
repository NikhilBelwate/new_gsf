package com.quinnox.epod.service;

import com.android.volley.VolleyError;
import com.quinnox.epod.beans.BaseGsonBean;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class ResponseHandler<T> {

    protected IResponse iResponse = null;

    protected int mRequestType = 0;

    public ResponseHandler(int argRequestType, IResponse argIResponse) {
        mRequestType = argRequestType;
        this.iResponse = argIResponse;
    }

    public void sendResponse(BaseGsonBean argBaseBean) {
        iResponse.onSuccessResponse(new ResponseEntity(mRequestType, argBaseBean));
    }

    public void sendResponse(String argResponse) {
        iResponse.onSuccessResponse(new ResponseEntity(mRequestType, argResponse));
    }

    public void reportError(VolleyError argError) {
        iResponse.onErrorResponse(new ResponseEntity(mRequestType, argError));
    }
}
