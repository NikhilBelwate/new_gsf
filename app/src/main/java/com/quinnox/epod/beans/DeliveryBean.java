package com.quinnox.epod.beans;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by dsen1 on 3/3/2018.
 */

public class DeliveryBean implements Parcelable,Serializable {
    private String desc;

    private double price;

    private String productid;

    private String comments;

    private int qty=0;

    private double totalPrice=0;

    private int uploadedQuantity=0;

    public DeliveryBean (){};

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice() {
        double calculatePrice = getUploadedQuantity() * getPrice();
        this.totalPrice = calculatePrice;
    }

    public int getUploadedQuantity() {
        return uploadedQuantity;
    }

    public void setUploadedQuantity(int uploadedQuantity) {
        this.uploadedQuantity = uploadedQuantity;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "ClassPojo<DeliveryBean> [desc = " + desc + ", price = " + price + ", productid = " + productid + ", qty = " + qty + "]";
    }

    protected DeliveryBean(Parcel in) {
        desc = in.readString();
        price = in.readDouble();
        productid = in.readString();
        comments = in.readString();
        qty = in.readInt();
        totalPrice = in.readDouble();
        uploadedQuantity = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(desc);
        dest.writeDouble(price);
        dest.writeString(productid);
        dest.writeString(comments);
        dest.writeInt(qty);
        dest.writeDouble(totalPrice);
        dest.writeInt(uploadedQuantity);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DeliveryBean> CREATOR = new Parcelable.Creator<DeliveryBean>() {
        @Override
        public DeliveryBean createFromParcel(Parcel in) {
            return new DeliveryBean(in);
        }

        @Override
        public DeliveryBean[] newArray(int size) {
            return new DeliveryBean[size];
        }
    };
}