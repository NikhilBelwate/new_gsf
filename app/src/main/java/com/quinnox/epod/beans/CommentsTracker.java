package com.quinnox.epod.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsen1 on 3/3/2018.
 */

public interface CommentsTracker {

    public void onCommentsAdded(ArrayList<DeliveryBean> deliveryBeanList, int position);
    void onFinish(Class classobj);
    void setTotat(double totat);
}
