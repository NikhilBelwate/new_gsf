package com.quinnox.epod.beans;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class TemperatureBean extends BaseGsonBean {

    private Main main;
    private String name;

    public String getName () {  return name; }

    public void setName (String name) { this.name = name; }

    public Main getMain () { return main; }

    public void setMain (Main main) { this.main = main; }
}
