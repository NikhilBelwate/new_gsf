package com.quinnox.epod.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.quinnox.epod.app.EpodApp;

/**
 * This is the Database utility class.
 * Contains convenience methods to interact with the database.
 * Created by Shabbir on 10-07-2015.
 */
public class DBUtil {

    private static SQLiteDatabase getDB()throws Exception {
        try {
            DBHelper dbHelper = new DBHelper(EpodApp.getContext());
            return dbHelper.getWritableDatabase();
        } catch (Exception e) {
            throw e;
        }
    }

    private static void closeCursor(Cursor argCursor){
        try{
            argCursor.close();
        }catch(Exception e){}
    }

    private static void closeDB(SQLiteDatabase argDB){
        try {
            argDB.close();
        }catch (Exception e ){}
    }

    public static void myMethod(){
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = getDB();
            // get cursor , db operations here (insert, update, delete)
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            closeCursor(cursor);
            closeDB(db);
        }
    }
}
