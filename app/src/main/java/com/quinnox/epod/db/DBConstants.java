package com.quinnox.epod.db;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class DBConstants {

    public final static int DATABASE_VERSION	= 1;
    public final static String	DATABASE_NAME = "ePOD.db";
}
