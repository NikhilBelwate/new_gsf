package com.quinnox.epod.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.InputStream;

/**
 * Created by Shabbir on 20-07-2015.
 */
public class EpodHttpClient {

    private Context mContext;

    public EpodHttpClient(Context context) {
        mContext = context;
    }

    public final boolean isInternetOn() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public InputStream doGet(String url) throws Exception {

        HttpParams httpParameters = new BasicHttpParams();
        HttpClient httpClient = HttpUtils.getNewHttpClient(true, httpParameters);
        HttpGet request = new HttpGet(url);
        request.setHeader(HTTP.CONTENT_TYPE, "application/json");
        HttpResponse response = null;
        try {
            response = httpClient.execute(request);
        } catch (Exception e) {
            Log.d("Network", "doGet Network Exception - " + e.getMessage() + " for URL - " + url);
            throw new Exception(e.getCause());
        }

        int statusCode = response.getStatusLine().getStatusCode();
        android.util.Log.d("Network-doGet()", "statusCode..." + statusCode);

        if (statusCode > 300) {
            Log.d("Network", "response is null..");
            throw new Exception("Response is NULL or Empty");
        } else {
            Log.d("Network", "got response.. ");

			/*for(Header header : response.getHeaders("Content-Length")) {
                System.out.println("Location from connect:" + header.getValue());*/

            return response.getEntity().getContent();
        }
    }
}

