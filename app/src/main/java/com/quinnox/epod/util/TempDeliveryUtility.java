package com.quinnox.epod.util;

import android.util.Log;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shabbir on 20-07-2015.
 */
public class TempDeliveryUtility {

    private static TempDeliveryUtility object;

    private final HashMap<String, ProductDetails> detailsHashMap = new HashMap<>();
    public static Set<Integer> completedRoutes = new HashSet<Integer>();

    private TempDeliveryUtility() {
        detailsHashMap.put("1110001", new ProductDetails("Reg Steek", 5, 2));
        //  detailsHashMap.put("1110002", new ProductDetails("Desc for product...", 2, 3));
        detailsHashMap.put("1110003", new ProductDetails("Large Steek", 10, 1));
        //  detailsHashMap.put("1110004", new ProductDetails("Desc for product...", 4, 2));
        detailsHashMap.put("1110005", new ProductDetails("French Fries", 20, 3));
    }

    public static TempDeliveryUtility getInstance() {
        return (object != null) ? object : new TempDeliveryUtility();
    }

    public boolean isBarCodeValid(String argBarCode) {
        return (detailsHashMap.containsKey(argBarCode)) ? true : false;
    }

    public String getDescription(String argBarCode) {
        return detailsHashMap.get(argBarCode).getDesc();
    }

    public Integer getTotalQty(String argBarCode) {
        return detailsHashMap.get(argBarCode).getTotalQty();
    }

    public Integer getPrice(String argBarCode) {
        return detailsHashMap.get(argBarCode).getUnitPrice();
    }

    public Integer getTotalPrice(String argBarCode) {
        return detailsHashMap.get(argBarCode).getTotalPrice();
    }

    public Integer getInvoiceTotal() {
        Integer total = 0;
        for (String key : detailsHashMap.keySet()) {
            total = total + detailsHashMap.get(key).getTotalPrice();
            Log.d("test", "key::" + detailsHashMap.get(key).getTotalPrice() + "total::" + total);
        }
        return total;
    }

    private class ProductDetails {
        String desc;
        Integer unitPrice;
        Integer totalQty;

        ProductDetails(String argDesc, Integer argUnitPrice, Integer argTotalQty) {
            desc = argDesc;
            unitPrice = argUnitPrice;
            totalQty = argTotalQty;
        }

        private String getDesc() {
            return desc;
        }

        private Integer getUnitPrice() {
            return unitPrice;
        }

        private Integer getTotalQty() {
            return totalQty;
        }

        private Integer getTotalPrice() {
            return totalQty * unitPrice;
        }

    }

    private static RouteDetails routeDetails = new RouteDetails();

    public void setRouteNo(String argRouteNo) {
        routeDetails.routeNo = argRouteNo;
    }

    public void setClientNo(String argNo) {
        routeDetails.clientNo = argNo;
    }

    public void setAddress(String argAddress) {
        routeDetails.address = argAddress;
    }

    public void setScheduledStartTime(String argTime) {
        routeDetails.scheduledStartTime = argTime;
    }

    public void setScheduledArrivalTime(String argTime) {
        routeDetails.scheduledArrivalTime = argTime;
    }

    public void setActualStartTime(String argTime) {
        routeDetails.actualStartTime = argTime;
    }

    public void setActualArrivalTime(String argTime) {
        routeDetails.actualArrivalTime = argTime;
    }

    public String getRouteNo() {
        return routeDetails.routeNo;
    }

    public String getClientNo() {
        return routeDetails.clientNo;
    }

    public String getAddress() {
        return routeDetails.address;
    }

    public String getScheduledStartTime() {
        return routeDetails.scheduledStartTime;
    }

    public String getScheduledArrivalTime() {
        return routeDetails.scheduledArrivalTime;
    }

    public String getActualStartTime() {
        return routeDetails.actualStartTime;
    }

    public String getActualArrivalTime() {
        return routeDetails.actualArrivalTime;
    }

    private static class RouteDetails {
        String routeNo = "";
        String scheduledStartTime = "";
        String scheduledArrivalTime = "";
        String actualStartTime = "";
        String actualArrivalTime = "";
        String clientNo = "";
        String address = "";
    }
}
