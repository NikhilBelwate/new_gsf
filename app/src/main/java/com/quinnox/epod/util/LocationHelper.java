package com.quinnox.epod.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by Shabbir on 15-07-2015.
 */
public class LocationHelper {

    private float latitude = 0.0f;
    private float longitude = 0.0f;

    private boolean gotLocation = false;

    private LocationManager locationManager;
    private MyLocationListener locationListener;

    public LocationHelper(Context context) {

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {

            latitude = (float) location.getLatitude();
            longitude = (float) location.getLongitude();

            locationManager.removeUpdates(locationListener);

            gotLocation = true;
        }

        public void onProviderDisabled(String provider) { }

        public void onProviderEnabled(String provider) { }

        public void onStatusChanged(String provider, int status, Bundle extras) { }
    }

    public void killLocationServices() {
        locationManager.removeUpdates(locationListener);
    }

    public float getLat() {
        return latitude;
    }

    public float getLong() {
        return longitude;
    }

    public Boolean gotLocation() {
        return gotLocation;
    }
}
