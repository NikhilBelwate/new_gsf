package com.quinnox.epod.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 * Created by Shabbir on 10-07-2015.
 */
public class Util {

    public static String convertInputStreamToString(InputStream is) {
        StringBuilder sb = new StringBuilder();
        String strResponse = null;

        if (is != null) {
            BufferedReader buff = new BufferedReader(new InputStreamReader(is));
            String sTemp = "";
            try {
                if (buff != null) {
                    while ((sTemp = buff.readLine()) != null) {
                        sb.append(sTemp);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (sb != null) {
            strResponse = sb.toString();
        }
        return strResponse;
    }

    public static String CelciustoFarenheit(String strTemp) {
        Double fTemp = Double.valueOf(strTemp);
        Double retTemp = round(fTemp*1.8+32, 2);
        return ""+retTemp;
    }

    public static String KMToMiles(String kmValue) {
        Double fKm = Double.valueOf(kmValue);
        Double retMiles = round(fKm/1.609344,2);
        return ""+retMiles;
    }

    private static Double round(Double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue() ;
    }
}
