package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.client.android.Intents;
import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.ui.adapter.RecyclerAdapter;
import com.quinnox.epod.ui.adapter.ReturnRecyclerAdapter;
import com.quinnox.epod.util.TempDeliveryUtility;

/**
 * Created by AradhanaV on 7/24/2015.
 */
public class ReturnsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ReturnRecyclerAdapter returnRecyclerAdapter;
    private Button btnSignOff, btnScan;

    private EditText edit_qty1_ret, edit_qty1_prod_desc, edit_qty2_ret, edit_qty2_prod_desc, edit_qty3_ret, edit_qty3_prod_desc,edit_qty1_prod_barcode,edit_qty2_prod_barcode,edit_qty3_prod_barcode;
    private String qty1_ret, qty2_ret, qty3_ret, qty1_detail, qty2_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_returns);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ImageView imageView = (ImageView)findViewById(R.id.arrow_icon);
        imageView.setVisibility(View.GONE);
        btnScan=(Button)findViewById(R.id.scan);
        btnSignOff=(Button)findViewById(R.id.btn_done);
        _wireEvents();

        returnRecyclerAdapter = new ReturnRecyclerAdapter(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(returnRecyclerAdapter);
/*
        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();*/
    }


    private void _wireEvents() {

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", Intents.Scan.MODE);
                    //     intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 0);

                } catch (Exception e) {
                    //          Log.v("ReturnsActivity", "scan e::" + e);
                }

            }
        });

        btnSignOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ReturnsActivity.this, SignatureCaptureActivity.class));
              //  finish();
            }
        });
    }


}

