package com.quinnox.epod.ui;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteBean;
import com.quinnox.epod.ui.view.CircularProgressBar;
import com.quinnox.epod.util.Stopwatch;

public class DeliveryProgressActivity extends AppCompatActivity {
    private TextView informationView, startTime, endTime, stopAddress, message, clientId;
    private ImageView showmap, showcall, showmsg, showchat;
    private Button arrivalBtn;
    private static String duration;
    private boolean isTimeOverdue = false;
    RouteBean routeBean = null;
    CircularProgressBar c1;
    int count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_progress);
        //_init();
        //_wireEvents();
        ImageView imageView = (ImageView)findViewById(R.id.arrow_icon);
        imageView.setVisibility(View.GONE);
        arrivalBtn=(Button)findViewById(R.id.btn_arrival);
        arrivalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DeliveryProgressActivity.this,DelayActivity.class);
                startActivity(intent);
            }
        });
        //EpodApp.setActionBar(this);
/*
        c1 = (CircularProgressBar) findViewById(R.id.circularprogressbar1);
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                c1.setProgress(count);
                c1.setTitle(count+"");
                count++;
                if(count==100){
                    count=0;
                }
            }

            public void onFinish() {
                //mTextField.setText("done!");
            }
        }.start();*/
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.temperature:
                startActivity(new Intent(this, TruckTemperatureActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void _wireEvents() {
        arrivalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.sendEmptyMessage(MSG_STOP_TIMER);
                /*Intent intent = new Intent(DeliveryProgressActivity.this, TimerActivity.class);
                intent.putExtra("route", DeliveryProgressActivity.this.getIntent().getSerializableExtra("route"));
                Log.e("duration", DeliveryProgressActivity.duration);
                intent.putExtra("duration", DeliveryProgressActivity.duration);
                if(isTimeOverdue)
                    Toast.makeText(DeliveryProgressActivity.this,"Time Overdue",Toast.LENGTH_LONG).show();
                startActivity(intent);*/
            }
        });

        showmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeliveryProgressActivity.this, DeliveryComponentDetails.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                intent.putExtra(DeliveryComponentDetails.TYPE, "Map");
                startActivity(intent);
                //finish();
            }
        });
        showcall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryProgressActivity.this);
                builder.setMessage(R.string.str_call_number);
                builder.setTitle("Call");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        String phone = "tel:" + getResources().getString(R.string.str_call_number);
                        intent.setData(Uri.parse(phone));
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //   finish();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        showmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:?subject=" + "Test email" + "&to=" + "testinfo@gsf.com");
                intent.setData(data);

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "No email app found", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    final int MSG_START_TIMER = 0;
    final int MSG_STOP_TIMER = 1;
    final int MSG_UPDATE_TIMER = 2;

    Stopwatch timer;
    final int REFRESH_RATE = 100;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START_TIMER:
                    timer.start(); //start timer
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    break;

                case MSG_UPDATE_TIMER:
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, REFRESH_RATE); //text view is updated every second,

                    break;                                  //though the timer is still running
                case MSG_STOP_TIMER:
                    mHandler.removeMessages(MSG_UPDATE_TIMER); // no more updates.

                    DeliveryProgressActivity.duration = adjustTime(timer.getElapsedTimeHour()) + ":" + adjustTime(timer.getElapsedTimeMin()) +
                            ":" + adjustTime(timer.getElapsedTimeSecs());
                    checkTimeOverdue();
                    timer.stop();//stop timer
                    Intent intent = new Intent(DeliveryProgressActivity.this, TimerActivity.class);
                    intent.putExtra("route", DeliveryProgressActivity.this.getIntent().getSerializableExtra("route"));
                    Log.v("duration1", DeliveryProgressActivity.duration);
                    intent.putExtra("duration", DeliveryProgressActivity.duration);
                    Log.v("Check", "isTimeOverdue:" + isTimeOverdue + "duration:" + duration);
                    if (isTimeOverdue) {
                        intent = new Intent(DeliveryProgressActivity.this, DelayActivity.class);
                        intent.putExtra("route", DeliveryProgressActivity.this.getIntent().getSerializableExtra("route"));
                        Log.v("duration2", DeliveryProgressActivity.duration);
                        intent.putExtra("duration", DeliveryProgressActivity.duration);
                        //  Toast.makeText(DeliveryProgressActivity.this, "Time Overdue", Toast.LENGTH_LONG).show();
                    }
                    DeliveryProgressActivity.this.startActivity(intent);
                    break;

                default:
                    break;
            }
        }

        public void checkTimeOverdue() {
            /*if(message.getText().toString().equals(arrivalTime.getText().toString()+":00"))
            {
                message.setBackgroundResource(android.R.color.holo_red_dark);
            }*/
            Log.e("duration", DeliveryProgressActivity.duration);
            String[] splitStartTime = routeBean.getStarttime().split(":");
            String[] splitArrivalTime = routeBean.getArrivaltime().split(":");
            int hours = Integer.parseInt(splitArrivalTime[0]) - Integer.parseInt(splitStartTime[0]);
            int min = Integer.parseInt(splitArrivalTime[1]) - Integer.parseInt(splitStartTime[1]);
            if (hours < timer.getElapsedTimeHour()) {
                isTimeOverdue = true;
            } else {
                //to check if arrived mins is greater than expected arrival
                // if (min <= timer.getElapsedTimeMin() && hours <= timer.getElapsedTimeHour()) {

                // adding wrong condition to show delay activity for first route
                if (min <= timer.getElapsedTimeMin()) {
                    isTimeOverdue = true;
                } else {
                    isTimeOverdue = false;
                }
            }

        }

        public String adjustTime(long l) {
            if ((l / 10) > 0)
                return "" + l;
            else
                return "0" + l;
        }
    };
}
