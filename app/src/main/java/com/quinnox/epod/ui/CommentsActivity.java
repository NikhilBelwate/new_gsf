package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.beans.DeliveryBean;

import java.util.ArrayList;

/**
 * Created by dsen1 on 3/3/2018.
 */

public class CommentsActivity extends AppCompatActivity {
    private EditText commentEdit;
    private ImageButton commentClose;
    private Button save;
    private ArrayList<DeliveryBean> deliveryBeanList;
    private int position;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_box);
        this.setFinishOnTouchOutside(false);
        Bundle bundle = getIntent().getExtras();
        commentEdit = (EditText)findViewById(R.id.commentEdit);
        commentClose = (ImageButton)findViewById(R.id.commentClose);
        if (bundle!=null){
            deliveryBeanList = bundle.getParcelableArrayList("deliverybean");
            position = bundle.getInt("position");
            if (deliveryBeanList.get(position).getComments()!=null){
                commentEdit.setText(deliveryBeanList.get(position).getComments());
            }
        }

        commentClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });
        save = (Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (commentEdit.getText().toString().trim().length()>0) {
                    deliveryBeanList.get(position).setComments(commentEdit.getText().toString().trim());
                    Intent intent = new Intent();
                    intent.putParcelableArrayListExtra("deliverybean", deliveryBeanList);
                    setResult(RESULT_OK, intent);
                    finish();
                }else Toast.makeText(CommentsActivity.this, "Please enter comments to save", Toast.LENGTH_LONG).show();
            }
        });
    }
}
