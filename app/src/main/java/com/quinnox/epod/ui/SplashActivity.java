package com.quinnox.epod.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.TemperatureBean;
import com.quinnox.epod.service.IResponse;
import com.quinnox.epod.service.ResponseEntity;
import com.quinnox.epod.service.ServiceManager;
import com.quinnox.epod.util.LocationHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SplashActivity extends Activity implements IResponse {

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private TextView mTxtTemp;
    private TextView mTextLocation, mDateTime;

    private GestureDetectorCompat gestureDetectorCompat;
    private LocationFinderAsyncTask locationFinderAsyncTask;

    private String locationProviders = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mTxtTemp = (TextView) findViewById(R.id.txt_temp);
    //    mTextLocation = (TextView) findViewById(R.id.text_place);
        mDateTime = (TextView) findViewById(R.id.txt_date_time);
        //to detect swipe on splash screen
        gestureDetectorCompat = new GestureDetectorCompat(this, new SwipeGestureListener());

        Calendar cal = Calendar.getInstance();
        Log.d("SplashActivity", "date and time " + new SimpleDateFormat("EEEE hh:mm a | MMM dd ,yyyy").format(cal.getTime()));
        mDateTime.setText(new SimpleDateFormat("EEEE hh:mm a  MMM dd yyyy").format(cal.getTime()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        String locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        Log.d("SHABS", "locationProviders : " + locationProviders);
        if (locationProviders.contains("gps") || locationProviders.contains("network")) {
            locationFinderAsyncTask = new LocationFinderAsyncTask();
            locationFinderAsyncTask.execute();
        } else {
            enableGPS();
        }
    }

    private void enableGPS() {

        new AlertDialog.Builder(this)
                .setMessage("No Location Services Enabled")
                .setPositiveButton("Enable Location Services",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                startActivity(new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                .setNegativeButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        }).create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public void onSuccessResponse(ResponseEntity argResponse) {
        if (ResponseEntity.REQUEST_TEMPERATURE == argResponse.getRequestType()) {
            TemperatureBean temperatureBean = (TemperatureBean) argResponse.getGsonBean();
            Log.d("SplashActivity", "Response : Location - " + temperatureBean.getName() + " Temp - " + temperatureBean.getMain().getTemp());
            mTxtTemp.setText("|"+((TemperatureBean) argResponse.getGsonBean()).getMain().getTemp() + (char) 0x00B0 + "F");
       //     mTxtTemp.setTypeface(null, Typeface.NORMAL);
       //     mTextLocation.setText(temperatureBean.getName());
        }
    }

    @Override
    public void onErrorResponse(ResponseEntity argError) {
        Log.d("SplashActivity", "ErrorResponse : " + argError.toString());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class SwipeGestureListener extends GestureDetector.SimpleOnGestureListener {
        //handle 'swipe ' action
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {

            //to detect swipe
            if (event2.getX() < event1.getX() || event2.getX() > event1.getX()) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
            return true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationFinderAsyncTask != null) {
            locationFinderAsyncTask.cancel(true);
        }
    }

    class LocationFinderAsyncTask extends AsyncTask<Boolean, Integer, Boolean> {

        LocationHelper locationHelper = new LocationHelper(EpodApp.getContext());

        @Override
        protected void onPreExecute() {}

        @Override
        protected Boolean doInBackground(Boolean... params) {

            while (locationHelper.gotLocation() == false && !isCancelled()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) { }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.d("SHABS", "Lat : " + locationHelper.getLat() + " Long : " + locationHelper.getLong());
            ServiceManager.getInstance().getTemperature("lat=" + locationHelper.getLat() + "&lon=" + locationHelper.getLong(), SplashActivity.this);
        }
    }

}
