package com.quinnox.epod.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.quinnox.epod.R;

public class LoginActivity extends Activity{

    private EditText edt_txt_driverId,edt_password;
    TextView hint_id,hint_password;
    private Button btn_login;
    ImageView warning1,warning2;
    private String driverId, driverPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //to initialize all components
        _init();
        // wire all events
        _wireEvents();

    }
    //method to initialize all components
    private void _init() {
        edt_txt_driverId=(EditText)findViewById(R.id.edt_text_driver_id);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btn_login=(Button)findViewById(R.id.btn_login);
        hint_id =(TextView) findViewById(R.id.hint_id);
        hint_password= (TextView)findViewById(R.id.hint_password);
        warning1=(ImageView)findViewById(R.id.warning1);
        warning2=(ImageView)findViewById(R.id.warning2);
        driverId= edt_txt_driverId.getText().toString().trim();
        driverPassword= edt_password.getText().toString().trim();



    }

    //method to wire all events
    public void _wireEvents() {
        edt_txt_driverId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    driverId= edt_txt_driverId.getText().toString().trim();
                    driverPassword= edt_password.getText().toString().trim();
                    // code to execute when EditText loses focus
                    if(driverId.length() <= 0 )
                    {
                        hint_id.setText("Driver ID required");
                        hint_id.setTextColor(getResources().getColor(R.color.error_red));
                        hint_id.setVisibility(View.VISIBLE);
                        warning1.setVisibility(View.VISIBLE);

                    }
                    if(driverId.length() > 0 &&  !driverId.equals("john"))
                    {
                        hint_id.setText("The Driver ID is incorrect");
                        hint_id.setTextColor(getResources().getColor(R.color.error_red));
                        hint_id.setVisibility(View.VISIBLE);
                        warning1.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
        edt_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    // code to execute when EditText loses focus
                    driverId= edt_txt_driverId.getText().toString().trim();
                    driverPassword= edt_password.getText().toString().trim();
                    if(driverPassword.length() < 0 )
                    {
                        hint_password.setText("Password required ");
                        hint_password.setTextColor(getResources().getColor(R.color.error_red));
                        hint_password.setVisibility(View.VISIBLE);
                        warning2.setVisibility(View.VISIBLE);
                    }
                    if(driverPassword.length() > 0  && !driverPassword.equals("123"))
                    {
                        hint_password.setText("Password is incorrect");
                        hint_password.setTextColor(getResources().getColor(R.color.error_red));
                        hint_password.setVisibility(View.VISIBLE);
                        warning2.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driverId= edt_txt_driverId.getText().toString().trim();
                driverPassword= edt_password.getText().toString().trim();
              /*  if(edtuser.getText().toString().trim().length()==0){
                    edtuser.setError("Username is not entered");
                    edtuser.requestFocus();
                }*/



                if ( driverId.equals("john") && driverPassword.equals("123")) {
                    Log.v("LoginActivity", "login successful ");
                    Intent intent=new Intent(LoginActivity.this,PreCheckListActivity.class);
                    startActivity(intent);
                    finish();
                } else {

                    hint_id.setText("Username or Password is incorrect");
                    hint_id.setTextColor(getResources().getColor(R.color.error_red));
                    hint_id.setVisibility(View.VISIBLE);
                    warning1.setVisibility(View.VISIBLE);
                    hint_password.setText("Username or Password is incorrect");
                    hint_password.setTextColor(getResources().getColor(R.color.error_red));
                    hint_password.setVisibility(View.VISIBLE);
                    warning2.setVisibility(View.VISIBLE);
                }


            }
        });
    }

}
