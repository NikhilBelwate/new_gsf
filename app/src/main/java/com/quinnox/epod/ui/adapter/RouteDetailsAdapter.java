package com.quinnox.epod.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteBean;
import com.quinnox.epod.util.TempDeliveryUtility;

import java.util.ArrayList;

/**
 * Created by NikhilB on 13-07-2015.
 */
public class RouteDetailsAdapter extends ArrayAdapter<RouteBean> {
    private ArrayList<RouteBean> routes;
    private View preSeleted;
    private RouteBean selectedItemData;


    public RouteDetailsAdapter(Context context, ArrayList<RouteBean> routes) {
        super(context, R.layout.route_list_row_item);
        this.routes = routes;
        preSeleted = null;
        selectedItemData = null;

    }

    @Override
    public int getCount() {
        return routes.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vH = null;

        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater) EpodApp.getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.route_list_row_item, parent, false);

            vH = new ViewHolder();

            vH.routeNo = (TextView) convertView.findViewById(R.id.txt_route_num);
            vH.startTime = (TextView) convertView.findViewById(R.id.txt_start_time);
            vH.arrivalTime = (TextView) convertView.findViewById(R.id.txt_arrival_time);
            vH.clientId = (TextView) convertView.findViewById(R.id.txt_client_id);
            vH.clientAddress = (TextView) convertView.findViewById(R.id.txt_client_address);
            vH.markIcon = (ImageView) convertView.findViewById(R.id.mark_icon);

            vH.routeNo.setText(routes.get(position).getStop());
            vH.clientAddress.setText(routes.get(position).getAddress() + "\n" + routes.get(position).getTelno());
            vH.clientId.setText(routes.get(position).getClientid());
            vH.startTime.setText(routes.get(position).getStarttime());
            vH.arrivalTime.setText(routes.get(position).getArrivaltime());
            if (TempDeliveryUtility.completedRoutes.contains(position + 1)) {
                vH.markIcon.setVisibility(View.VISIBLE);
                convertView.setEnabled(false);
            } else {
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (preSeleted != null) {
                            preSeleted.setBackgroundColor(Color.WHITE);
                        }
                        v.setBackgroundColor(Color.GRAY);

                        preSeleted = v;
                    }
                });
            }
            convertView.setTag(vH);

        } else {
            vH = (ViewHolder) convertView.getTag();
        }


        return convertView;
    }

    public RouteBean getSelectedItemData() {
        if (preSeleted != null) {
            ViewHolder viewHolder = (ViewHolder) preSeleted.getTag();
            int position = Integer.parseInt(viewHolder.routeNo.getText().toString());
            selectedItemData = routes.get(position - 1);
        }
        return selectedItemData;
    }


    private class ViewHolder {
        public TextView clientAddress;
        public TextView clientId;
        public TextView startTime;
        public TextView arrivalTime;
        public TextView routeNo;
        public ImageView markIcon;
    }

}
