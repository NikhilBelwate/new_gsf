package com.quinnox.epod.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;

/**
 * Created by AradhanaV on 7/15/2015.
 */
public class PreCheckListActivity extends EpodBaseActivity {
        TextView t1;
    Button b1, b2, precheck;
    ListView l1;
    private DrawerLayout drawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_check_list);
        String[] data = {"Perform DOT Pre-trip inspection on tractor and trailer", "Check fuel in tractor and trailer",
                "Verify current tractor and trailer inspection stickers and tags"}; //how many items you want
        t1 = (TextView) findViewById(R.id.text01);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        precheck = (Button) findViewById(R.id.next);
        l1 = (ListView) findViewById(R.id.listView);
        CustomAdapter ad = new CustomAdapter(this, data);
        l1.setAdapter(ad);

//      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
//
//        setSupportActionBar(toolbar);
//
//        //create default navigation drawer toggle
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
//                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        //    drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//
//
//
//
//        //handling navigation view item event
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        assert navigationView != null;
//        navigationView.setNavigationItemSelectedListener(this);

        precheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PreCheckListActivity.this, RouteSummaryActivity.class);
                startActivity(intent);
            }
        });
    }
  /*  private void gotoStartJourneyActivity(){
        mBtnRouteList=(Button)findViewById(R.id.btn_route_list);
        mBtnRouteList.setOnClickListener(new View.OnClickListener(){
     @Override
      public void onClick(View v){
        startActivity(new Intent(PreCheckListActivity.this,StartJourneyActivity.class));
        }
        });


        }*/
//  @Override
//  public boolean onNavigationItemSelected(MenuItem item) {
//      int id = item.getItemId();
//
//      if (id == R.id.fr1) {
//          Intent okokok = new Intent(Intent.ACTION_VIEW);
//          okokok.setData(Uri.parse("http://www.gmail.com"));
//          startActivity(okokok);
//
//      } else if (id == R.id.fr2) {
//          Intent okokok = new Intent(Intent.ACTION_VIEW);
//          okokok.setData(Uri.parse("http://www.facebook.com"));
//          startActivity(okokok);
//
//      } else if (id == R.id.fr3) {
//          Intent okokok = new Intent(Intent.ACTION_VIEW);
//          okokok.setData(Uri.parse("http://www.youtube.com"));
//          startActivity(okokok);
//
//      } else if (id == R.id.go) {
//          Intent okokok = new Intent(Intent.ACTION_VIEW);
//          okokok.setData(Uri.parse("http://www.pinterest.com"));
//          startActivity(okokok);
//      } else if (id == R.id.close) {
//          finish();
//      }
//
//
//
//
//
//      drawer.closeDrawer(GravityCompat.START);
//      return true;
//  }
//
//    @Override
//    public void onBackPressed() {
//        assert drawer != null;
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }


}
