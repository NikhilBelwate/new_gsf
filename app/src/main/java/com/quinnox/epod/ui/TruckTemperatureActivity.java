package com.quinnox.epod.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.constants.EpodConstants.JSONParams;
import com.quinnox.epod.service.ServiceManager;
import com.quinnox.epod.util.TempDeliveryUtility;
import com.quinnox.epod.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Shabbir on 16-07-2015.
 */
public class TruckTemperatureActivity extends Activity {

    private Button btnCancel;
    private Button btnSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.truck_temperature);
        EpodApp.setActionBar(this);

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

//        ServiceManager.getInstance().getTruckTemperature(this);
        if (EpodApp.isNetworkAvailable()) {
            TruckDetailsASyncTask task = new TruckDetailsASyncTask();
            task.execute("");
        }

        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();
        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getScheduledArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        _wireEvents();
    }

    private void _wireEvents() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TruckTemperatureActivity.this, "Sending truck information...", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private class TruckDetailsASyncTask extends AsyncTask<String, String, String> {

        ProgressDialog dialog = null;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(TruckTemperatureActivity.this, "", "Fetching details from iBright...", true);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... arg0) {
            String data = null;
            try {
                data = ServiceManager.getInstance().getTruckTemperature();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isCancelled())
                return data;
            else
                return "cancelled";
        }

        @Override
        protected void onPostExecute(String result) {
            if (dialog.isShowing())
                dialog.dismiss();
            if (result != null) {
                try {
                    String strLatitude = "", strLongitude = "", strOdometer = "", strFuelLevel = "";
                    String strChilledTemp = "", strChilledAAT = "", strChilledSet = "", strChilledSupply = "";
                    String strFrozenTemp = "", strFrozenAAT = "", strFrozenSet = "", strFrozenSupply = "";
                    JSONArray arrTruckDetails = new JSONArray(result);
                    for (int count = arrTruckDetails.length() - 1; count >= 0; count--) {
                        JSONObject obj = arrTruckDetails.getJSONObject(count).getJSONObject(JSONParams.DATA);
                        if (obj.get(JSONParams.NAME).equals("temperature1")) {
                            if (obj.has(JSONParams.TEMPERATURE)) {
                                strFrozenTemp = obj.getString(JSONParams.TEMPERATURE);
                                ((TextView) findViewById(R.id.frozen_temperature).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.temperature));
                                ((TextView) findViewById(R.id.frozen_temperature).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenTemp));
                            }
                            if (obj.has(JSONParams.AMBIENTAIR)) {
                                strFrozenAAT = obj.getString(JSONParams.AMBIENTAIR);
                                ((TextView) findViewById(R.id.frozen_ambientair).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.ambientair));
                                if(strFrozenSet.equals(""))
                                    ((TextView) findViewById(R.id.frozen_ambientair).findViewById(R.id.txt_value)).setText("19.12");
                                else
                                ((TextView) findViewById(R.id.frozen_ambientair).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenAAT));
                            }
                            if (obj.has(JSONParams.SETPOINT)) {
                                strFrozenSet = obj.getString(JSONParams.SETPOINT);
                                ((TextView) findViewById(R.id.frozen_setpoint).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.setpoint));
                                if(strFrozenSet.equals(""))
                                    ((TextView) findViewById(R.id.frozen_setpoint).findViewById(R.id.txt_value)).setText("89.63");
                                else
                                ((TextView) findViewById(R.id.frozen_setpoint).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenSet));
                            }
                            if (obj.has(JSONParams.SUPPLYAIR)) {
                                strFrozenSupply = obj.getString(JSONParams.SUPPLYAIR);
                                ((TextView) findViewById(R.id.frozen_supplyair).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.supplyair));
                                ((TextView) findViewById(R.id.frozen_supplyair).findViewById(R.id.txt_value)).setText(strFrozenSupply);
                            }else{
                                ((TextView) findViewById(R.id.frozen_supplyair).findViewById(R.id.txt_title)).setVisibility(View.GONE);
                                ((TextView) findViewById(R.id.frozen_supplyair).findViewById(R.id.txt_value)).setVisibility(View.GONE);
                            }
                        } else if (obj.get(JSONParams.NAME).equals("temperature2")) {
                            if (obj.has(JSONParams.TEMPERATURE)) {
                                strFrozenTemp = obj.getString(JSONParams.TEMPERATURE);
                                ((TextView) findViewById(R.id.chilled_temperature).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.temperature));
                                if(strFrozenSet.equals(""))
                                   ((TextView) findViewById(R.id.chilled_temperature).findViewById(R.id.txt_value)).setText("36.12");
                                else
                                ((TextView) findViewById(R.id.chilled_temperature).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenTemp));
                            }
                            if (obj.has(JSONParams.AMBIENTAIR)) {
                                strFrozenAAT = obj.getString(JSONParams.AMBIENTAIR);
                                ((TextView) findViewById(R.id.chilled_ambientair).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.ambientair));
                                if(strFrozenSet.equals(""))
                                    ((TextView) findViewById(R.id.chilled_ambientair).findViewById(R.id.txt_value)).setText("25.13");
                                else
                                ((TextView) findViewById(R.id.chilled_ambientair).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenAAT));
                            }
                            if (obj.has(JSONParams.SETPOINT)) {
                                strFrozenSet = obj.getString(JSONParams.SETPOINT);

                                Log.v("1test","obj: "+ obj.toString());
                                ((TextView) findViewById(R.id.chilled_setpoint).findViewById(R.id.txt_title)).setText(getResources().getText(R.string.setpoint));
                                if (strFrozenSet.equals(""))
                                    ((TextView) findViewById(R.id.chilled_setpoint).findViewById(R.id.txt_value)).setText("12.75");
                                else
                                    ((TextView) findViewById(R.id.chilled_setpoint).findViewById(R.id.txt_value)).setText(Util.CelciustoFarenheit(strFrozenSet));
                                String test=obj.getString(JSONParams.SETPOINT);
                                Log.v("1test","1test: "+test);
                            }
                        }
//                      else {
//                            if (strLatitude.equals("") && obj.has(JSONParams.LATITUDE)) {
//                                strLatitude = obj.getString(JSONParams.LATITUDE);
//                            }
//                            if (strLongitude.equals("") && obj.has(JSONParams.LONGITUDE)) {
//                                strLongitude = obj.getString(JSONParams.LONGITUDE);
//                            }
//                            if (strOdometer.equals("") && obj.has(JSONParams.ODOMETER)) {
//                                strOdometer = obj.getString(JSONParams.ODOMETER);
//                                txtOdometer.setText(util.KMToMiles(strOdometer));
//                            }
//                            if (strFuelLevel.equals("") && obj.has(JSONParams.REEFERFUELPERCERNT)) {
//                                strFuelLevel = obj.getString(JSONParams.REEFERFUELPERCERNT);
//                                txtFuelPercent.setText(strFuelLevel);
//                            }
//                        }
                    }

//                    if (txtFuelPercent.getText().toString().equals("")) {
//                        txtFuelPercent.setTextAppearance(getActivity(), R.style.old_value_label);
//                        txtFuelPercent.setText(prefs.getString(SharedPrefKeys.REEFERFUELPERCERNT, "0.0"));
//                    }
//                    if (txtOdometer.getText().toString().equals("")) {
//                        txtOdometer.setTextAppearance(getActivity(), R.style.old_value_label);
//                        txtOdometer.setText(prefs.getString(SharedPrefKeys.ODOMETER, "0.0"));
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}