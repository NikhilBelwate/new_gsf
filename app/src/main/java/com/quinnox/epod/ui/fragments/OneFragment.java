package com.quinnox.epod.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.artifex.mupdflib.MuPDFActivity;
import com.quinnox.epod.R;
import com.quinnox.epod.constants.EpodConstants;
import com.quinnox.epod.ui.DeliveryProgressActivity;
import com.quinnox.epod.ui.RouteDetailsActivity;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OneFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public Button  startJourney;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public ViewGroup innerView;
    private OnFragmentInteractionListener mListener;

    public OneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OneFragment newInstance(String param1, String param2) {
        OneFragment fragment = new OneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the delivary_details_card_layout for this fragment
        innerView= (ViewGroup) inflater.inflate(R.layout.fragment_one, container, false);
        startJourney=(Button)innerView.findViewById(R.id.start_journey);
        setTempData(innerView);
        startJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onStartJuornyClick(v);
            }
        });
        return innerView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public void setTempData(ViewGroup innerView){
        RelativeLayout address_details1=(RelativeLayout)innerView.findViewById(R.id.address_details1);
        ImageView imageView=(ImageView) address_details1.findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.pdf);
        ((TextView) address_details1.findViewById(R.id.route_name)).setText("Delivery_slip ");
        ((TextView) address_details1.findViewById(R.id.store_id)).setText("It contains main item of business");
        ((TextView) address_details1.findViewById(R.id.store_address)).setText("All information copied by web sources ");
        address_details1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MuPDFActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(EpodConstants.DELIVERYSLIPPDF)), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                getActivity().startActivity(intent);
            }
        });


        RelativeLayout address_details2=(RelativeLayout)innerView.findViewById(R.id.address_details2);
        ImageView imageView2=(ImageView) address_details2.findViewById(R.id.icon);
        imageView2.setImageResource(R.drawable.pdf);
        ((TextView) address_details2.findViewById(R.id.route_name)).setText("Credit memo");
        ((TextView) address_details2.findViewById(R.id.store_id)).setText("bill details and history ");
        ((TextView) address_details2.findViewById(R.id.store_address)).setText("All information copied by web sources ");
        address_details2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MuPDFActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(EpodConstants.CREDITMEMO)), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                getActivity().startActivity(intent);
            }
        });
        RelativeLayout address_details=(RelativeLayout)innerView.findViewById(R.id.address_details);
        //ImageView imageView0=(ImageView) address_details2.findViewById(R.id.icon);
        address_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Location Image...");

                // set the custom dialog components - text, image and button

                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                image.setBackgroundResource(R.drawable.hotel);

                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


/*
        RelativeLayout address_details3=(RelativeLayout)innerView.findViewById(R.id.address_details3);
        ImageView imageView3=(ImageView) address_details3.findViewById(R.id.icon);
        imageView3.setImageResource(R.drawable.excel_icon);
        ((TextView) address_details3.findViewById(R.id.route_name)).setText("Collection Figures");
        ((TextView) address_details3.findViewById(R.id.store_id)).setText("It is main rules of business ");
        ((TextView) address_details3.findViewById(R.id.store_address)).setText("All information copied by web sources ");*/
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onStartJuornyClick(View view);
    }
}
