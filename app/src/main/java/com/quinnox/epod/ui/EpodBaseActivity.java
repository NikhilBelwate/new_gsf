package com.quinnox.epod.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.quinnox.epod.R;

/**
 * Created by Shabbir on 16-07-2015.
 */
public class EpodBaseActivity extends Activity {

    private static String reason="";
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.temperature:
                startActivity(new Intent(this, TruckTemperatureActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public static String getReason() {
        return reason;
    }

    public static void setReason(String reason) {
        EpodBaseActivity.reason = reason;
    }

}
