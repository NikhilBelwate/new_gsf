package com.quinnox.epod.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.beans.RouteBean;
import com.quinnox.epod.ui.DeliveryProgressActivity;
import com.quinnox.epod.ui.DeliverySummaryActivity;
import com.quinnox.epod.ui.RouteSummaryActivity;

import java.util.ArrayList;

/**
 * Created by NikhilB on 01/03/2018.
 */

public class RouteCardAdapter extends Adapter<RouteCardAdapter.MyViewHolder> {
    private ArrayList<RouteBean> routes;
    private Context context;
    OnRouteSelectListener routeListener;
    public RouteCardAdapter(Context context, ArrayList<RouteBean> routes, OnRouteSelectListener routeListener){
        this.context=context;
        this.routes=routes;
        this.routeListener=routeListener;
    }
    public interface OnRouteSelectListener{
        public void routeSelected(RouteBean routeBean);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView routeId, routeName, storetId, address;
        public ImageView arrowImage;
        public FloatingActionButton fab;
        public MyViewHolder(View itemView) {
            super(itemView);
            routeId=(TextView)itemView.findViewById(R.id.route_no);
            routeName=(TextView)itemView.findViewById(R.id.route_name);
            storetId=(TextView)itemView.findViewById(R.id.store_id);
            address=(TextView)itemView.findViewById(R.id.store_address);
            arrowImage=(ImageView)itemView.findViewById(R.id.arrow_icon);
            fab=(FloatingActionButton)itemView.findViewById(R.id.fab);
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.route_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.routeName.setText(routes.get(position).getAddress());
        holder.address.setText(routes.get(position).getAddress()+",HU-state,USA");
        holder.routeId.setText(routes.get(position).getStop());
        holder.storetId.setText("storet id-"+routes.get(position).getTelno());
        if(DeliverySummaryActivity.routeId>=Integer.parseInt(routes.get(position).getStop())){
            holder.fab.setBackgroundTintList(context.getResources().getColorStateList(R.color.back_green));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.itemView.setBackgroundColor(Color.GRAY);
                holder.itemView.setBackgroundColor(Color.WHITE);
                routeListener.routeSelected(routes.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }
}
