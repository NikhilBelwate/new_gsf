package com.quinnox.epod.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artifex.mupdflib.FilePicker;
import com.artifex.mupdflib.MuPDFActivity;
import com.artifex.mupdflib.MuPDFCore;
import com.artifex.mupdflib.MuPDFPageAdapter;
import com.artifex.mupdflib.MuPDFReaderView;
import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.constants.EpodConstants;
import com.quinnox.epod.util.TempDeliveryUtility;

import java.io.File;

/**
 * Created by Shabbir on 21-07-2015.
 */
public class DeliverySummaryActivity extends AppCompatActivity {

    public static int routeId=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_summary);
        getSupportActionBar().setHomeButtonEnabled(true);
        WebView webView = (WebView)findViewById(R.id.loadInvoice);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.getharvest.com/assets/resources/invoice-template-pdf-lg-4b85256873302a50e49138597d3d9d1bb1ef4ee2b73466f6dbe883152c699432.png");

        File imgFile = new File(Environment.getExternalStorageDirectory(), EpodConstants.APP_DIRECTORY
                + File.separator + "truckDriver.png");
        Bitmap bitmap;
        if (imgFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ((ImageView) findViewById(R.id.signature_driver)).setImageBitmap(bitmap);
        }
        imgFile = new File(Environment.getExternalStorageDirectory(), EpodConstants.APP_DIRECTORY
                + File.separator + "storeManager.png");
        if (imgFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ((ImageView) findViewById(R.id.signature_store_manager)).setImageBitmap(bitmap);
        }

        Button back=(Button)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DeliverySummaryActivity.this,RouteSummaryActivity.class);
                routeId++;
                intent.putExtra("routeId",routeId);
                startActivity(intent);
            }
        });
        /*findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeliverySummaryActivity.this, RouteSummaryActivity.class);
                intent.putExtra("cycle", "end");
                intent.putExtra("routeNo", TempDeliveryUtility.getInstance().getRouteNo());
                startActivity(intent);
                finish();
            }
        });

        EpodApp.setActionBar(this);
        populateUI();*/
    }

    /*private void populateUI() {
        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.scheduled_start_time).findViewById(R.id.txt_title)).setText(R.string.scheduled_start_time);
        ((TextView) findViewById(R.id.actual_start_time).findViewById(R.id.txt_title)).setText(R.string.actual_start_time);
        ((TextView) findViewById(R.id.scheduled_arrival_time).findViewById(R.id.txt_title)).setText(R.string.scheduled_arrival_time);
        ((TextView) findViewById(R.id.actual_arrival_time).findViewById(R.id.txt_title)).setText(R.string.actual_arrival_time);

        ((TextView) findViewById(R.id.txt_route_num)).setText(tempDeliveryUtility.getRouteNo());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        ((TextView) findViewById(R.id.scheduled_start_time).findViewById(R.id.txt_value)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.actual_start_time).findViewById(R.id.txt_value)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.scheduled_arrival_time).findViewById(R.id.txt_value)).setText(tempDeliveryUtility.getScheduledArrivalTime());
        ((TextView) findViewById(R.id.actual_arrival_time).findViewById(R.id.txt_value)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_reason)).setText(getReason());

        File imgFile = new File(Environment.getExternalStorageDirectory(), EpodConstants.APP_DIRECTORY
                + File.separator + "truckDriver.png");
        Bitmap bitmap;
        if (imgFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ((ImageView) findViewById(R.id.signature_driver)).setImageBitmap(bitmap);
        }
        imgFile = new File(Environment.getExternalStorageDirectory(), EpodConstants.APP_DIRECTORY
                + File.separator + "storeManager.png");
        if (imgFile.exists()) {
            bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ((ImageView) findViewById(R.id.signature_store_manager)).setImageBitmap(bitmap);
        }
        LinearLayout invoiceLayout = (LinearLayout) findViewById(R.id.invoice_pdf_content);
        LinearLayout returnsLayout = (LinearLayout) findViewById(R.id.returns_pdf_content);

        try {
            MuPDFCore core = new MuPDFCore(getApplicationContext(), EpodConstants.PDFPATH);
            MuPDFReaderView invoicePDFView = new MuPDFReaderView(this);

            invoicePDFView.setAdapter(new MuPDFPageAdapter(this, new FilePicker.FilePickerSupport() {
                @Override
                public void performPickFor(FilePicker picker) {

                }
            }, core));
            invoiceLayout.addView(invoicePDFView);

            MuPDFReaderView returnsPDFView = new MuPDFReaderView(this);
            returnsPDFView.setAdapter(new MuPDFPageAdapter(this, new FilePicker.FilePickerSupport() {
                @Override
                public void performPickFor(FilePicker picker) {

                }
            }, core));
            returnsLayout.addView(returnsPDFView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) findViewById(R.id.invoice_label)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPdf();
            }
        });
        ((TextView) findViewById(R.id.returns_label)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPdf();
            }
        });
    }

    public void viewPdf() {
        Intent intent = new Intent(DeliverySummaryActivity.this, MuPDFActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(EpodConstants.PDFPATH)), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }*/
}
