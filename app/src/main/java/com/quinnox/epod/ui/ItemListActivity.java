package com.quinnox.epod.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.quinnox.epod.R;
import com.quinnox.epod.beans.CommentsTracker;
import com.quinnox.epod.beans.DeliveryBean;
import com.quinnox.epod.ui.adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ItemListActivity extends AppCompatActivity implements CommentsTracker {
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private ArrayList<DeliveryBean> deliveryBeanList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ImageView imageView = (ImageView)findViewById(R.id.arrow_icon);
        imageView.setVisibility(View.GONE);
        prepareDeliveryBean();
    }
    private void prepareDeliveryBean(){

        deliveryBeanList = new ArrayList<>();

        DeliveryBean deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Meat Packet");
        deliveryBean.setPrice(10);
        deliveryBean.setProductid("ASD909876");
        deliveryBean.setQty(10);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Rice Packet");
        deliveryBean.setPrice(20);
        deliveryBean.setProductid("QWE909876");
        deliveryBean.setQty(10);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Wheat Packet");
        deliveryBean.setPrice(110);
        deliveryBean.setProductid("DFG909876");
        deliveryBean.setQty(20);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Beer Packet");
        deliveryBean.setPrice(210);
        deliveryBean.setProductid("BEE909876");
        deliveryBean.setQty(10);
        deliveryBeanList.add(deliveryBean);

        recyclerAdapter = new RecyclerAdapter(deliveryBeanList, this,1);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.temperature:
                startActivity(new Intent(this, TruckTemperatureActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onCommentsAdded(ArrayList<DeliveryBean> deliveryBeanList, int position) {
        Intent intent = new Intent(ItemListActivity.this, CommentsActivity.class);
        intent.putParcelableArrayListExtra("deliverybean", deliveryBeanList);
        intent.putExtra("position", position);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onFinish(Class classobj) {
        Intent intent=new Intent(this,classobj);
        startActivity(intent);
    }

    @Override
    public void setTotat(double totat) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101){
            if(resultCode == RESULT_OK) {
                deliveryBeanList = data.getParcelableArrayListExtra("deliverybean");
                recyclerAdapter.refreshList(deliveryBeanList);
            }
        }
    }
}
