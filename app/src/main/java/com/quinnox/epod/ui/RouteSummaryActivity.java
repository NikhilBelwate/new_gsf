package com.quinnox.epod.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteBean;
import com.quinnox.epod.beans.RouteDetailsBean;
import com.quinnox.epod.service.IResponse;
import com.quinnox.epod.service.ResponseEntity;
import com.quinnox.epod.service.ServiceManager;
import com.quinnox.epod.ui.adapter.RouteCardAdapter;
import com.quinnox.epod.ui.adapter.RouteDetailsAdapter;
import com.quinnox.epod.util.TempDeliveryUtility;

import java.util.ArrayList;

/**
 * Created by AradhanaV on 7/9/2015.
 */
public class RouteSummaryActivity extends AppCompatActivity implements IResponse, RouteCardAdapter.OnRouteSelectListener {

    private ListView listv_route_summary;
    private TextView txt_route_num;

    private Button btn_start_journey;
    private boolean endOfDayFlag;
    //private RouteSummaryAdapter routeSummaryAdapter;
    private RouteDetailsAdapter routeDetailsAdapter;
    private ProgressDialog dialog = null;
    private RecyclerView recyclerView;
    private RouteCardAdapter routeCardAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_summary);
        //to initialize all components
        _init();
        // wire all events
        //_wireEvents();
        //EpodApp.setActionBar(this);
        if (isInternetOn()) {
            ServiceManager.getInstance().getRoutes(this);
            dialog = ProgressDialog.show(RouteSummaryActivity.this, "", "Fetching route info...", true);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
        } else {
            networkAlert();
        }
    }




    //method to initialize all components
    private void _init() {

        txt_route_num = (TextView) findViewById(R.id.txt_route_num);

       // btn_start_journey = (Button) findViewById(R.id.btn_start_journey);
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        endOfDayFlag = false;
        routeDetailsAdapter = null;
        try {
            if (getIntent().getStringExtra("cycle").toString().equals("end")) {
                int routeno = Integer.parseInt(getIntent().getStringExtra("routeNo"));
                TempDeliveryUtility.completedRoutes.add(routeno);
            }
        } catch (Exception e) {

        }

    }


    //method to wire all events
    public void _wireEvents() {
        btn_start_journey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (endOfDayFlag) {

                    Intent intent = new Intent(RouteSummaryActivity.this, EndOfDayActivity.class);
                    startActivity(intent);
                } else {
                    if (routeDetailsAdapter != null) {
                        if (routeDetailsAdapter.getSelectedItemData() != null) {
                            Intent intent = new Intent(RouteSummaryActivity.this, StartJourneyActivity.class);
                            intent.putExtra("route", routeDetailsAdapter.getSelectedItemData());
                            startActivity(intent);

                            RouteBean selectedItemData = routeDetailsAdapter.getSelectedItemData();
                            TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();
                            tempDeliveryUtility.setRouteNo(selectedItemData.getStop());
                            tempDeliveryUtility.setScheduledStartTime(selectedItemData.getStarttime());
                            tempDeliveryUtility.setScheduledArrivalTime(selectedItemData.getArrivaltime());
                            tempDeliveryUtility.setClientNo(selectedItemData.getClientid());
                            tempDeliveryUtility.setAddress(selectedItemData.getAddress() + "\n" + selectedItemData.getTelno());
                        }
                    }
                }
                // finish();

            }
        });
    }


    @Override
    public void onSuccessResponse(ResponseEntity argResponseEntity) {
        if (dialog.isShowing())
            dialog.dismiss();
       // listv_route_summary = (ListView) findViewById(R.id.listv_route_summary);
        ArrayList<RouteBean> listData = ((RouteDetailsBean) argResponseEntity.getGsonBean()).getData();
        if (TempDeliveryUtility.completedRoutes.size() == listData.size()) {
            btn_start_journey.setText("End of Day Activity");
            endOfDayFlag = true;
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayout.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        routeCardAdapter=new RouteCardAdapter(getApplicationContext(),listData, this);
        recyclerView.setAdapter(routeCardAdapter);
        routeDetailsAdapter = new RouteDetailsAdapter(getApplicationContext(), listData);
       // listv_route_summary.setAdapter(routeDetailsAdapter);
    }

    @Override
    public void onErrorResponse(ResponseEntity argResponseEntity) {
        if (dialog.isShowing())
            dialog.dismiss();

        Toast.makeText(RouteSummaryActivity.this, "Server error. Please try after some time.", Toast.LENGTH_SHORT).show();
    }

    private void networkAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Internet connectivity not available");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (isInternetOn()) {
                    ServiceManager.getInstance().getRoutes(RouteSummaryActivity.this);
                } else {
                    Toast.makeText(RouteSummaryActivity.this, "Internet connectivity not available", Toast.LENGTH_LONG).show();
                    networkAlert();
                }
            }
        })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public final boolean isInternetOn() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @Override
    public void routeSelected(RouteBean routeBean) {
        Intent intent=new Intent(this, RouteDetailsActivity.class);
        intent.putExtra("route",routeBean);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.temperature:
                startActivity(new Intent(this, TruckTemperatureActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
