package com.quinnox.epod.ui.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.beans.CommentsTracker;
import com.quinnox.epod.beans.DeliveryBean;
import com.quinnox.epod.ui.DeliveryDetailsActivity;

import java.util.ArrayList;

/**
 * Created by NikhilB on 05/03/2018.
 */

public class ReturnRecyclerAdapter extends RecyclerView.Adapter<ReturnRecyclerAdapter.CardViewHolder> {

private ArrayList<DeliveryBean> deliveryBeanList;
private Context context;
private CommentsTracker commentsTracker;
private int flag;
public static int count;
public static double total;


public class CardViewHolder extends RecyclerView.ViewHolder {

    EditText barcode, description, quantity;
   // LinearLayout priceTextHolder;
    ImageButton commentClose;
    FloatingActionButton addComment;
    Button save;

    public CardViewHolder(View view) {
        super(view);
        barcode=(EditText)view.findViewById(R.id.productId);
        description=(EditText)view.findViewById(R.id.expectedPrice);
        quantity=(EditText)view.findViewById(R.id.uploadedPrice);

        addComment = (FloatingActionButton) view.findViewById(R.id.addComment);
        //priceTextHolder=(LinearLayout)view.findViewById(R.id.priceTextHolder);

    }
}


    public ReturnRecyclerAdapter(Context context) {
        this.deliveryBeanList = deliveryBeanList;
        this.context = context;

    }

    @Override
    public ReturnRecyclerAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.returns, parent, false);
        return new ReturnRecyclerAdapter.CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ReturnRecyclerAdapter.CardViewHolder holder, final int position) {
        //final DeliveryBean deliveryBean = deliveryBeanList.get(position);

       /* holder.productId.setText(deliveryBean.getProductid());
        holder.productDescription.setText(deliveryBean.getDesc());
        holder.expectedQuantity.setText(Integer.toString(deliveryBean.getQty()));
        holder.expectedPrice.setText(Double.toString(deliveryBean.getPrice()));
        holder.productDescription.setText(deliveryBean.getDesc());
        if(flag==0){
            holder.uploadedQuantity.setText(holder.expectedQuantity.getText());
            deliveryBean.setUploadedQuantity(deliveryBean.getQty());
            deliveryBean.setTotalPrice();
            holder.uploadedQuantity.setEnabled(false);


            holder.uploadedPrice.setText("$"+deliveryBean.getTotalPrice());
            total=total+deliveryBean.getTotalPrice();
            commentsTracker.setTotat(total);
        }
        holder.uploadedQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int uploaded=0;
                try {
                    uploaded= Integer.parseInt(s.toString());
                }catch(Exception e){
                    uploaded=0;
                }
                if(uploaded==Integer.parseInt(holder.expectedQuantity.getText().toString())){
                    count++;
                    holder.uploadedQuantity.setEnabled(false);
                    if(count==4){
                        commentsTracker.onFinish(DeliveryDetailsActivity.class);
                    }
                }

            }
        });
        if (deliveryBean.getComments()!=null){
            holder.addComment.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(android.R.color.holo_green_dark)));
        }
        holder.uploadedQuantity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0!=null&&arg0.toString().length()>0){
                    deliveryBean.setUploadedQuantity(Integer.parseInt(arg0.toString()));
                    deliveryBean.setTotalPrice();
                    holder.uploadedPrice.setText(Double.toString(deliveryBean.getTotalPrice()));
                }
            }
        });

        if(flag==1){
            holder.expectedPrice.setVisibility(View.INVISIBLE);
            holder.uploadedPrice.setVisibility(View.INVISIBLE);
            holder.priceTextHolder.setVisibility(View.INVISIBLE);
        }
        else{
            holder.expectedPrice.setVisibility(View.VISIBLE);
            holder.uploadedPrice.setVisibility(View.VISIBLE);
            holder.priceTextHolder.setVisibility(View.VISIBLE);
        }
        holder.addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentsTracker.onCommentsAdded(deliveryBeanList, position);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return 1;
    }




}

