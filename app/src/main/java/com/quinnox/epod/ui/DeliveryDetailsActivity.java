package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.CommentsTracker;
import com.quinnox.epod.beans.DeliveryBean;
import com.quinnox.epod.ui.adapter.RecyclerAdapter;
import com.quinnox.epod.util.TempDeliveryUtility;

import java.util.ArrayList;

/**
 * Created by AradhanaV on 7/20/2015.
 */
public class DeliveryDetailsActivity extends AppCompatActivity implements CommentsTracker {

    private Button btnBack,btnDone;
    private TextView qty1_delivered, qty2_delivered, qty3_delivered;
    private TextView qty1Desc, qty1Barcode, qty2Desc, qty2Barcode, qty3Desc, qty3Barcode;
    private TextView qty1Price, qty2Price, qty3Price;
    private TextView qty1Total, qty2Total, qty3Total;
    private String mtxtBarcode1, mtxtBarcode2, mtxtBarcode3;
    private TextView txt_total;
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private ArrayList<DeliveryBean> deliveryBeanList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_details);
        txt_total=(TextView)findViewById(R.id.total);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ImageView imageView = (ImageView)findViewById(R.id.arrow_icon);
        imageView.setVisibility(View.GONE);
        prepareDeliveryBean();
       /* TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());*/

        //to initialize all components
        //_init();
        // wire all events
        _wireEvents();


    }
    private void prepareDeliveryBean(){

        deliveryBeanList = new ArrayList<>();

        DeliveryBean deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Meat Packet");
        deliveryBean.setPrice(10);
        deliveryBean.setProductid("ASD909876");
        deliveryBean.setQty(5);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Rice Packet");
        deliveryBean.setPrice(20);
        deliveryBean.setProductid("QWE909876");
        deliveryBean.setQty(15);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Wheat Packet");
        deliveryBean.setPrice(110);
        deliveryBean.setProductid("DFG909876");
        deliveryBean.setQty(20);
        deliveryBeanList.add(deliveryBean);

        deliveryBean = new DeliveryBean();
        deliveryBean.setDesc("Beer Packet");
        deliveryBean.setPrice(210);
        deliveryBean.setProductid("BEE909876");
        deliveryBean.setQty(8);
        deliveryBeanList.add(deliveryBean);

        recyclerAdapter = new RecyclerAdapter(deliveryBeanList, this,0);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.temperature:
                startActivity(new Intent(this, TruckTemperatureActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onCommentsAdded(ArrayList<DeliveryBean> deliveryBeanList, int position) {
        Intent intent = new Intent(DeliveryDetailsActivity.this, CommentsActivity.class);
        intent.putParcelableArrayListExtra("deliverybean", deliveryBeanList);
        intent.putExtra("position", position);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onFinish(Class classobj) {
        Intent intent=new Intent(this,classobj);
        startActivity(intent);
    }

    @Override
    public void setTotat(double totat) {
        txt_total.setText("$"+totat);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101){
            if(resultCode == RESULT_OK) {
                deliveryBeanList = data.getParcelableArrayListExtra("deliverybean");
                recyclerAdapter.refreshList(deliveryBeanList);
            }
        }
    }


    private void _wireEvents() {
        btnBack=(Button)findViewById(R.id.btn_back);
        btnDone=(Button)findViewById(R.id.btn_done);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DeliveryDetailsActivity.this, ReturnsActivity.class));
              //  finish();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DeliveryDetailsActivity.this, SignatureCaptureActivity.class));
                //  finish();
            }
        });

    }

}
