package com.quinnox.epod.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.Intents;
import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.util.TempDeliveryUtility;

/**
 * Created by AradhanaV on 7/17/2015.
 */
public class CountAndScanModeActivity extends EpodBaseActivity {

    private Button mBtnDone, mBtnScan;
    private EditText qty1_delivered, qty2_delivered, qty3_delivered;
    private TextView qty1_to_be_del, qty2_to_be_del, qty3_to_be_del;
    private TextView txt_barcode1, txt_barcode2, txt_barcode3;
    private TextView txt_qty1_desc, txt_qty2_desc, txt_qty3_desc;
    private String txtQty1ToBeDel, txtQty2ToBeDel, txtQty3ToBeDel, txtQty1Del, txtQty2Del, txtQty3Del, mtxtBarcode1, mtxtBarcode2, mtxtBarcode3;
    private int qty1Count = 0, qty2Count = 0, qty3Count = 0;


    String pre = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_scan_mode);
        EpodApp.setActionBar(this);

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();


    }

    private void _init() {

        mBtnDone = (Button) findViewById(R.id.btn_done);
        mBtnScan = (Button) findViewById(R.id.btn_scan);

        qty1_delivered = (EditText) findViewById(R.id.edit_qty1_del);
        qty2_delivered = (EditText) findViewById(R.id.edit_qty2_del);
        qty3_delivered = (EditText) findViewById(R.id.edit_qty3_del);

        qty1_to_be_del = (TextView) findViewById(R.id.txt_qty1_to_be_del);
        qty2_to_be_del = (TextView) findViewById(R.id.txt_qty2_to_be_del);
        qty3_to_be_del = (TextView) findViewById(R.id.txt_qty3_to_be_del);


        txt_barcode1 = (TextView) findViewById(R.id.txt_barcode1);
        txt_barcode2 = (TextView) findViewById(R.id.txt_barcode2);
        txt_barcode3 = (TextView) findViewById(R.id.txt_barcode3);

        txt_qty1_desc = (TextView) findViewById(R.id.txt_qty1_desc);
        txt_qty2_desc = (TextView) findViewById(R.id.txt_qty2_desc);
        txt_qty3_desc = (TextView) findViewById(R.id.txt_qty3_desc);

        TempDeliveryUtility tempDeliveryUtility = TempDeliveryUtility.getInstance();

        ((TextView) findViewById(R.id.txt_start_time)).setText(tempDeliveryUtility.getScheduledStartTime());
        ((TextView) findViewById(R.id.txt_arrival_time)).setText(tempDeliveryUtility.getActualArrivalTime());
        ((TextView) findViewById(R.id.txt_client_id)).setText(tempDeliveryUtility.getClientNo());
        ((TextView) findViewById(R.id.txt_client_address)).setText(tempDeliveryUtility.getAddress());

        //    RouteBean routeBean = (RouteBean) getIntent().getSerializableExtra("route");

        //to get the text stored in edit text and qty to be delivered
        txtQty1Del = qty1_delivered.getText().toString();
        txtQty1ToBeDel = qty1_to_be_del.getText().toString();
        mtxtBarcode1 = txt_barcode1.getText().toString();

        txtQty2Del = qty2_delivered.getText().toString();
        txtQty2ToBeDel = qty2_to_be_del.getText().toString();
        mtxtBarcode2 = txt_barcode2.getText().toString();


        txtQty3Del = qty3_delivered.getText().toString();
        txtQty3ToBeDel = qty3_to_be_del.getText().toString();
        mtxtBarcode3 = txt_barcode3.getText().toString();

      /*  stopAddress.setText(routeBean.getAddress() + "\n" + routeBean.getTelno());
        startTime.setText(routeBean.getStarttime());
        endTime.setText(routeBean.getArrivaltime());
        clientId.setText(routeBean.getClientid());
*/
        //to display data from TempDeliveryUtility class
        txt_qty1_desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode1));
        txt_qty2_desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode2));
        txt_qty3_desc.setText(tempDeliveryUtility.getDescription(mtxtBarcode3));

        qty1_to_be_del.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode1)));
        qty2_to_be_del.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode2)));
        qty3_to_be_del.setText(String.valueOf(tempDeliveryUtility.getTotalQty(mtxtBarcode3)));


    }

    private void _wireEvents() {


        //to change text color of edit text after changing text
        qty1_delivered.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                qty1_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        qty2_delivered.addTextChangedListener(new TextWatcher() {


            @Override
            public void afterTextChanged(Editable s) {
                qty2_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        qty3_delivered.addTextChangedListener(new TextWatcher() {


            @Override
            public void afterTextChanged(Editable s) {
                qty3_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });


        mBtnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", Intents.Scan.MODE);
                    //     intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 0);

                } catch (Exception e) {
                    Log.v("CountScanModeActivity", "scan e::" + e);
                }
            }
        });

        mBtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = true;
                txtQty1Del = qty1_delivered.getText().toString();
                txtQty1ToBeDel = qty1_to_be_del.getText().toString();

                txtQty2Del = qty2_delivered.getText().toString();
                txtQty2ToBeDel = qty2_to_be_del.getText().toString();

                txtQty3Del = qty3_delivered.getText().toString();
                txtQty3ToBeDel = qty3_to_be_del.getText().toString();

                if (!(txtQty1Del.equals(txtQty1ToBeDel))) {
                    isValid = false;
                    qty1_delivered.setTextColor(getResources().getColor(R.color.error_red));
                } else {
                    qty1_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));
                }
                if (!(txtQty2Del.equals(txtQty2ToBeDel))) {
                    isValid = false;
                    qty2_delivered.setTextColor(getResources().getColor(R.color.error_red));
                } else {
                    qty2_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));
                }
                if (!(txtQty3Del.equals(txtQty3ToBeDel))) {
                    isValid = false;
                    qty3_delivered.setTextColor(getResources().getColor(R.color.error_red));
                } else {
                    qty3_delivered.setTextColor(getResources().getColor(R.color.primary_theme_color));
                }

                if (qty1_delivered.getText().toString().equals("") || qty2_delivered.getText().toString().equals("") || qty3_delivered.getText().toString().equals(""))
                    Toast.makeText(getApplicationContext(), "Please enter Loaded qty details", Toast.LENGTH_LONG).show();
                else {
                    if ((txtQty1Del.equals(txtQty1ToBeDel)) && (txtQty2Del.equals(txtQty2ToBeDel)) && (txtQty3Del.equals(txtQty3ToBeDel))) {
                        Intent intent = new Intent(CountAndScanModeActivity.this, DeliveryDetailsActivity.class);
                        startActivity(intent);
                       // finish();
                    }
                }
            }
        });


    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.v("CountScanModeActivity", "intent:: " + intent);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                txtQty1ToBeDel = qty1_to_be_del.getText().toString();
                txtQty2ToBeDel = qty2_to_be_del.getText().toString();
                txtQty3ToBeDel = qty3_to_be_del.getText().toString();

                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.v("CountScanModeActivity", "Scanned contents is::" + contents + " format: " + format);


                //to check for first product
                try {
                    if (contents.equals(txt_barcode1.getText().toString())) {
                        txtQty1Del = qty1_delivered.getText().toString();
                        if (qty1Count < Integer.parseInt(txtQty1ToBeDel) && Integer.parseInt(qty1_delivered.getText().toString()) < Integer.parseInt(txtQty1ToBeDel)) {
                            qty1Count = Integer.parseInt(txtQty1Del) + 1;
                            qty1_delivered.setText(Integer.toString(qty1Count));
                        } else {
                            Toast.makeText(getApplicationContext(), "Delivery of " + contents + " Completed!!", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    qty1Count = 1;
                    qty1_delivered.setText(Integer.toString(qty1Count));
                }
                //to check for second product
                try {
                    if (contents.equals(txt_barcode2.getText().toString())) {
                        txtQty2Del = qty2_delivered.getText().toString();
                        if (qty2Count < Integer.parseInt(txtQty2ToBeDel) && Integer.parseInt(qty2_delivered.getText().toString()) < Integer.parseInt(txtQty2ToBeDel)) {
                            qty2Count = Integer.parseInt(txtQty2Del) + 1;
                            qty2_delivered.setText(Integer.toString(qty2Count));
                        } else {
                            Toast.makeText(getApplicationContext(), "Delivery of " + contents + " Completed!!", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    qty2Count = 1;
                    qty2_delivered.setText(Integer.toString(qty2Count));
                }

                //to check for third product
                try {
                    if (contents.equals(txt_barcode3.getText().toString())) {
                        txtQty3Del = qty3_delivered.getText().toString();
                        if (qty3Count < Integer.parseInt(txtQty3ToBeDel) && Integer.parseInt(qty3_delivered.getText().toString()) < Integer.parseInt(txtQty3ToBeDel)) {
                            qty3Count = Integer.parseInt(txtQty3Del) + 1;
                            qty3_delivered.setText(Integer.toString(qty3Count));
                        } else {
                            Toast.makeText(getApplicationContext(), "Delivery of " + contents + " Completed!!", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    qty3Count = 1;
                    qty3_delivered.setText(Integer.toString(qty3Count));
                }


            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
                Log.i("CountScanModeActivity", "Scan unsuccessful");
            }
        }


    }

}
