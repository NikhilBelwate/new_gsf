package com.quinnox.epod.ui.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Movie;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;
import com.quinnox.epod.beans.CommentsTracker;
import com.quinnox.epod.beans.DeliveryBean;
import com.quinnox.epod.ui.DeliveryDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by dsen1 on 3/3/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CardViewHolder> {

    private ArrayList<DeliveryBean> deliveryBeanList;
    private Context context;
    private CommentsTracker commentsTracker;
    private int flag;
    public static int count;
    public static double total;


    public class CardViewHolder extends RecyclerView.ViewHolder {
        TextView productId, productDescription, expectedQuantity, expectedPrice, uploadedPrice;
        EditText uploadedQuantity, commentEdit;
        LinearLayout priceTextHolder;
        ImageButton commentClose;
        FloatingActionButton addComment;
        Button save;

        public CardViewHolder(View view) {
            super(view);
            productId = (TextView) view.findViewById(R.id.productId);
            productDescription = (TextView) view.findViewById(R.id.productDescription);
            expectedQuantity = (TextView) view.findViewById(R.id.expectedQuantity);
            expectedPrice = (TextView) view.findViewById(R.id.expectedPrice);
            uploadedPrice = (TextView) view.findViewById(R.id.uploadedPrice);
            uploadedQuantity = (EditText) view.findViewById(R.id.uploadedQuantity);
            addComment = (FloatingActionButton) view.findViewById(R.id.addComment);
            priceTextHolder=(LinearLayout)view.findViewById(R.id.priceTextHolder);

        }
    }


    public RecyclerAdapter(ArrayList<DeliveryBean> deliveryBeanList, Context context, int flag) {
        this.deliveryBeanList = deliveryBeanList;
        this.context = context;
        commentsTracker = (CommentsTracker)context;
        this.flag=flag;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_details, parent, false);
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, final int position) {
        final DeliveryBean deliveryBean = deliveryBeanList.get(position);

        holder.productId.setText(deliveryBean.getProductid());
        holder.productDescription.setText(deliveryBean.getDesc());
        holder.expectedQuantity.setText(Integer.toString(deliveryBean.getQty()));
        holder.expectedPrice.setText(Double.toString(deliveryBean.getPrice()));
        holder.productDescription.setText(deliveryBean.getDesc());
        if(flag==0){
            holder.uploadedQuantity.setText(holder.expectedQuantity.getText());
            deliveryBean.setUploadedQuantity(deliveryBean.getQty());
            deliveryBean.setTotalPrice();
            holder.uploadedQuantity.setEnabled(false);


            holder.uploadedPrice.setText("$"+deliveryBean.getTotalPrice());
            total=total+deliveryBean.getTotalPrice();
            commentsTracker.setTotat(total);
        }
        holder.uploadedQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int uploaded=0;
                try {
                     uploaded= Integer.parseInt(s.toString());
                }catch(Exception e){
                    uploaded=0;
                }
                if(uploaded==Integer.parseInt(holder.expectedQuantity.getText().toString())){
                    count++;
                    holder.uploadedQuantity.setEnabled(false);
                    if(count==4){
                        commentsTracker.onFinish(DeliveryDetailsActivity.class);
                    }
                }

            }
        });
        if (deliveryBean.getComments()!=null){
            holder.addComment.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(android.R.color.holo_green_dark)));
        }
        holder.uploadedQuantity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0!=null&&arg0.toString().length()>0){
                    deliveryBean.setUploadedQuantity(Integer.parseInt(arg0.toString()));
                    deliveryBean.setTotalPrice();
                    holder.uploadedPrice.setText(Double.toString(deliveryBean.getTotalPrice()));
                }
            }
        });

        if(flag==1){
            holder.expectedPrice.setVisibility(View.INVISIBLE);
            holder.uploadedPrice.setVisibility(View.INVISIBLE);
            holder.priceTextHolder.setVisibility(View.INVISIBLE);
        }
        else{
            holder.expectedPrice.setVisibility(View.VISIBLE);
            holder.uploadedPrice.setVisibility(View.VISIBLE);
            holder.priceTextHolder.setVisibility(View.VISIBLE);
        }
        holder.addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentsTracker.onCommentsAdded(deliveryBeanList, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (deliveryBeanList != null)
            return deliveryBeanList.size();
        else return 0;
    }

    public void refreshList(ArrayList<DeliveryBean> deliveryBeanList){
        this.deliveryBeanList = deliveryBeanList;
        notifyDataSetChanged();
    }


}
