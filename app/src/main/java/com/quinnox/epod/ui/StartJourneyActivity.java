package com.quinnox.epod.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quinnox.epod.R;
import com.quinnox.epod.app.EpodApp;
import com.quinnox.epod.beans.RouteBean;

/**
 * Created by AradhanaV on 7/16/2015.
 */
public class StartJourneyActivity extends Activity {

    Button mBtnContinueJourney;
    LinearLayout llDeliveryContent, llDeliveryDoc, llRestDetails, llSpecificInfo;
    RouteBean routeBean = null;
    private TextView startTime, endTime, stopAddress, clientId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_journey);
        EpodApp.setActionBar(this);

        //to initialize all components
        _init();
        // wire all events
        _wireEvents();

        gotoStartActivity();

    }

    private void _init() {
        mBtnContinueJourney = (Button) findViewById(R.id.btn_continue_journey);
        llDeliveryContent = (LinearLayout) findViewById(R.id.ll_delivery_content);
        llDeliveryDoc = (LinearLayout) findViewById(R.id.ll_delivery_document);
        llRestDetails = (LinearLayout) findViewById(R.id.ll_restaurant_details);
        llSpecificInfo = (LinearLayout) findViewById(R.id.ll_specific_info);
        getIntent().getSerializableExtra("route");

        startTime = (TextView)findViewById(R.id.txt_start_time);
        endTime = (TextView) findViewById(R.id.txt_arrival_time);
        clientId = (TextView) findViewById(R.id.txt_client_id);
        stopAddress = (TextView) findViewById(R.id.txt_client_address);

        routeBean = (RouteBean) getIntent().getSerializableExtra("route");

        Log.v("test", "routebean::" + routeBean.getAddress());
        stopAddress.setText(routeBean.getAddress() + "\n" + routeBean.getTelno());
        startTime.setText(routeBean.getStarttime());
        endTime.setText(routeBean.getArrivaltime());
        clientId.setText(routeBean.getClientid());
    }


    private void gotoStartActivity() {
        mBtnContinueJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartJourneyActivity.this, DeliveryProgressActivity.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                startActivity(intent);

            }
        });
    }

    private void _wireEvents() {

        llDeliveryContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartJourneyActivity.this, DeliveryComponentDetails.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                intent.putExtra(DeliveryComponentDetails.TYPE, "Delivery Content");
                startActivity(intent);
               // finish();
            }
        });

        llDeliveryDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartJourneyActivity.this, DeliveryComponentDetails.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                intent.putExtra(DeliveryComponentDetails.TYPE, "Delivery Document");
                startActivity(intent);
               // finish();

            }
        });

        llRestDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartJourneyActivity.this, DeliveryComponentDetails.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                intent.putExtra(DeliveryComponentDetails.TYPE, "Restaurant Details");
                startActivity(intent);
               // finish();

            }
        });

        llSpecificInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartJourneyActivity.this, DeliveryComponentDetails.class);
                intent.putExtra("route", getIntent().getSerializableExtra("route"));
                intent.putExtra(DeliveryComponentDetails.TYPE, "Specific Info");
                startActivity(intent);
               // finish();
            }
        });
    }


}
