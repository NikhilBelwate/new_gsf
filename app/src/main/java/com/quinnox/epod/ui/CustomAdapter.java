package com.quinnox.epod.ui;

import android.content.ClipData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.quinnox.epod.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnurikaC on 22-Feb-18.
 */

public class CustomAdapter extends BaseAdapter {
    Context con;
    String[] data;
    Button b1,b2;
    TextView text;
    int  flag=0;
    static class ViewHolder {
        private TextView text;
        private Button b1,b2;
       // private ImageView personImageView;
    }

     public  void counter()
     {
      flag++;
     }
    public CustomAdapter(Context context, String[] data) {
        this.con = context;

        this.data = data;

    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //this method will be called for every item of your listview
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        convertView = inflater.inflate(R.layout.customview, parent, false);
//       text = (TextView) convertView.findViewById(R.id.text01);//recognize your view like this
//        b1=(Button) convertView.findViewById(R.id.b1);
//       b2=(Button) convertView.findViewById(R.id.b2);
//        text.setText(data[position]);
//
//               return convertView;
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.customview, null, false); // use sublayout which you want to inflate in your each list item

            holder.text = (TextView) convertView.findViewById(R.id.text01); // see you have to find id by using convertView.findViewById
            holder.text.setText(data[position]);
            holder.b1 = (Button) convertView.findViewById(R.id.b1);
            holder.b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Button)view).getBackground().setAlpha(128);
                    ((Button)view).setText("YES");
                    counter();
                    Log.d("countr value","is"+flag);
                }
            });

            holder.b2 = (Button) convertView.findViewById(R.id.b2);
            holder.b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Button)view).getBackground().setAlpha(128);
                    ((Button)view).setText("NO");
                     counter();
                     Log.d("countr value","is"+flag);
                }
            });
            convertView.setTag(holder);

        }
        else{
            holder = (ViewHolder) convertView.getTag();
          /*  holder.b1[position].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.getBackground().setAlpha(128);
                }
            });

            holder.b2[position] = (Button) convertView.findViewById(R.id.b2);
            holder.b2[position].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.getBackground().setAlpha(128);
                }
            });*/
        }


                return convertView;
            }


        }


